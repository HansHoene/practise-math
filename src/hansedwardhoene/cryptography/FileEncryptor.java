// Program: FileEncryptor
// Written by: Hans-Edward Hoene
// Description: Encrypts and decrypts files
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Feb 25, 2016        	 HEH       				Creation Began
// 3/1/2016				HEH						Creation Finished

package hansedwardhoene.cryptography;

import java.io.*;
import java.math.BigInteger;

/*filename: FileIO.java*/
public class FileEncryptor {
	
	@SuppressWarnings("unused")
	private final static int VERSION = 1;
	@SuppressWarnings("unused")
	private final static int UPDATE = 1;
	@SuppressWarnings("unused")
	private final static String DATE = "1 March 2016";
	
	public final static int BASE_TEN = 10;
	public final static int BINARY = 2;
	public final static int OCTAL = 8;
	public final static int HEXADECIMAL = 16;
	
	public final static char[] BIG_BASE_DIGITS = {'A','B','C','D','E','F'};
	
	private int base;
	private int publicKey;
	private int privateKey;
	private BigInteger modulus;
	
	private int numberLength;
	
	private EncryptionStatusListener[] listeners;
	
	public FileEncryptor(int base, int publicKey, int privateKey, int modulus, boolean checkKeys) throws InternalError, IllegalArgumentException {
		//validate input
		if (publicKey <= 1 || privateKey <= 1) {
			throw new IllegalArgumentException("Key values may not be less than two.");
		}
		if (base != BINARY && base != OCTAL && base != HEXADECIMAL && base != BASE_TEN) {
			throw new IllegalArgumentException("The base may only be 2, 8, 16, or 10 to represent numbers in base ten, binary, octal, and hexadecimal.");
		}
		if (modulus <= (Math.abs(Byte.MIN_VALUE) + Math.abs(Byte.MAX_VALUE))) {
			throw new IllegalArgumentException("Modulus must be greater than "+(Math.abs(Byte.MIN_VALUE) + Math.abs(Byte.MAX_VALUE))+" to support every byte value.");
		}
		
		this.base = base;
		this.publicKey = publicKey;
		this.privateKey = privateKey;
		this.modulus = BigInteger.valueOf(modulus);
		
		this.numberLength = 0;
		
		this.listeners = new EncryptionStatusListener[0];
		
		//find maximum number length necessary
		{
			//length is number length (amount of digits needed to fully represent modulus)
			for ( int length=1 ; true ; length++ ) {
				BigInteger sum = BigInteger.ZERO; //count sum of each base multiplied by (base - 1) ^ pos
				for ( int i=0 ; i<length ; i++ ) {
					sum = sum.add(BigInteger.valueOf(this.base - 1).multiply(BigInteger.valueOf(this.base).pow(i)));
				}
				if (sum.compareTo(this.modulus) >= 0) {
					sum = null;
					this.numberLength = length;
					break;
				}
				sum = null;
			}
		}
		
		if (this.numberLength == 0) {
			throw new InternalError("Unknown error ocurred");
		}
		
		//check if keys work
		if (checkKeys) {
			for ( int i=Byte.MIN_VALUE ; i<=Byte.MAX_VALUE ; i++ ) {
				//System.out.print();
				if (BigInteger.valueOf(i).compareTo(BigInteger.valueOf(i + Math.abs(Byte.MIN_VALUE)).pow(this.publicKey).mod(this.modulus).pow(this.privateKey).mod(this.modulus).subtract(BigInteger.valueOf(Math.abs(Byte.MIN_VALUE)))) != 0) {
					throw new IllegalArgumentException("Key values do not work.");
				}
			}
		}
		
	}
	
	public void encrypt(File file) throws FileNotFoundException, IOException, IllegalArgumentException {
		File tmp; //temporary file
		DataInputStream reader, tmpReader; // 1st: reads bytes from file -- 2nd: reads bytes from temp file
		BufferedWriter tmpWriter; //writes numbers to temp file
		DataOutputStream writer; //writes bytes to original file
		
		double bytesToTransfer = (numberLength + 1) * file.length(); //size of file * 2 because encrypt byte then copy
		double progress = 0; //progress or encrypting file
		int[] progressKept = new int[101];
		for ( int i=0 ; i<progressKept.length ; i++ ) {
			progressKept[i] = 0;
		}
		
		//validate file input, fire location event, create tmp file, encrypt byte stream to tmp file, copy data from tmp to original, delete tmp
		if (!file.exists()) {
			throw new FileNotFoundException("File not found.");
		}
		if (!file.isFile()) { //if folder, encrypt every file inside
			File[] innerFiles = file.listFiles();
			for ( int count=0 ; count<innerFiles.length ; count++ ) {
				encrypt(innerFiles[count]);
				innerFiles[count] = null;
			}
			innerFiles = null;
			return;
		}
		
		//fire location event
		locationChanged(file.getAbsolutePath());
		updateProgress(0);
		
		//create temporary file to temporarily store data
		tmp = File.createTempFile("~$TMP_Encryption -- "+file.getName(), ".tmp", file.getParentFile());
		
		//encrypt byte by byte to tmp file
		reader = new DataInputStream(new FileInputStream(file));
		tmpWriter = new BufferedWriter(new FileWriter(tmp));
		byte nextByte; //byte to read
		BigInteger encByte; //byte to encrypt
		String baseString; //string to write into file representing number with approprite base
		while (true) {
			try {
				//read byte, encrypt value, convert value to string representing number with appropriate base, write to tmp file
				
				nextByte = reader.readByte(); //read byte
				encByte = BigInteger.valueOf(nextByte + Math.abs(Byte.MIN_VALUE)).pow(publicKey).mod(modulus); //encrypt value
				
				//convert to string
				{
					baseString = "";
					for ( int pos=(numberLength - 1) ; pos >=0 ; pos-- ) { //loop through positions
						for ( int value=(base - 1) ; value >= 0 ; value-- ) { //loop through every possible value of num in position until it finds appropriate value
							BigInteger test = BigInteger.valueOf(value).multiply(BigInteger.valueOf(base).pow(pos)); //value of num * (base ^ pos)
							if (test.compareTo(encByte) <= 0) {
								encByte = encByte.subtract(test);
								if (value <= 9) {
									baseString += value;
								} else if (value <= 16) {
									baseString += BIG_BASE_DIGITS[value - 10];
								}
								break;
							}
						}
					}
				}
				
				//write to file
				tmpWriter.write(baseString);
				
				progress++;
				if (progressKept[(int)(progress * 100 / bytesToTransfer)] == 0) {
					progressKept[(int)(progress * 100 / bytesToTransfer)] = 1;
					updateProgress((int)(progress * 100 / bytesToTransfer));
				}
			} catch (EOFException end) { //if stream ends, come here
				break;
			}
		}
		progress = file.length();
		updateProgress((int)(progress * 100 / bytesToTransfer));
		reader.close();tmpWriter.close(); //close streams
		reader = null;tmpWriter = null; //set streams to null for garbage collection
		//time = System.currentTimeMillis() - timer;
		
		//copy data to original
		tmpReader = new DataInputStream(new FileInputStream(tmp));
		writer = new DataOutputStream(new FileOutputStream(file));
		
		byte b;
		//timer = System.currentTimeMillis();
		while (true) {
			try {
				b = tmpReader.readByte();
				writer.writeByte(b);
				
				progress++;
				if (progressKept[(int)(progress * 100 / bytesToTransfer)] == 0) {
					progressKept[(int)(progress * 100 / bytesToTransfer)] = 1;
					updateProgress((int)(progress * 100 / bytesToTransfer));
				}
			} catch (EOFException end) {
				break;
			}
		}
		updateProgress(100);
		tmpReader.close();writer.close();
		tmpReader = null;writer = null;
		
		//delete tmp file
		tmp.delete();
		tmp = null;
		
		file = null;
	}
	
	public void decrypt(File file) throws FileNotFoundException, IOException, IllegalArgumentException {
		File tmp; //temporary file
		DataInputStream reader, tmpReader; //readers
		DataOutputStream writer, tmpWriter; //writers
		
		double bytesToTransfer = (file.length() / numberLength) + file.length(); //size of file * 2 because encrypt byte then copy
		double progress = 0; //progress or encrypting file
		int[] progressKept = new int[101];
		for ( int i=0 ; i<progressKept.length ; i++ ) {
			progressKept[i] = 0;
		}
		
		//validate file input, fire location event, create tmp file, decrypt file by character group in to tmp file, copy data from tmp file to original, delete tmp file
		
		//validate input
		if (!file.exists()) {
			throw new FileNotFoundException("File not found.");
		}
		if (!file.isFile()) { //if folder, decrypt inner files
			File[] innerFiles = file.listFiles();
			for ( int count=0 ; count<innerFiles.length ; count++ ) {
				decrypt(innerFiles[count]);
			}
			innerFiles = null;
			return;
		}
		
		//fire location event
		locationChanged(file.getAbsolutePath());
		updateProgress(0);
		
		//create tmp file
		tmp = File.createTempFile("~$TMP_Decryption -- "+file.getName(), ".tmp", file.getParentFile());
		
		//decrypt file by character group
		reader = new DataInputStream(new FileInputStream(file));
		tmpWriter = new DataOutputStream(new FileOutputStream(tmp));
		char nextChar;
		int charToNum, sum;
		byte decByte;
		while (true) {
			try {
				sum = 0;
				for ( int i=0 ; i<numberLength ; i++ ) {
					//read byte long enough for number
					nextChar = (char)((reader.readByte()) & 0xFF);
					try {
						charToNum = Integer.parseInt(String.valueOf(nextChar));
					} catch (NumberFormatException err) {
						charToNum = 0;
						for ( int loop=0 ; loop<BIG_BASE_DIGITS.length ; loop++ ) {
							if (nextChar == BIG_BASE_DIGITS[loop]) {
								charToNum = loop + 10;
								break;
							}
						}
					}
					if (charToNum >= base) {
						throw new IllegalArgumentException("Illegal characters in file to decrypt.");
					}
					sum += (charToNum * Math.pow(base, numberLength - i - 1));
					
					progress++;
					if (progressKept[(int)(progress * 100 / bytesToTransfer)] == 0) {
						progressKept[(int)(progress * 100 / bytesToTransfer)] = 1;
						updateProgress((int)(progress * 100 / bytesToTransfer));
					}
				}
				decByte = BigInteger.valueOf(sum).pow(privateKey).mod(modulus).subtract(BigInteger.valueOf(Math.abs(Byte.MIN_VALUE))).byteValue();
				tmpWriter.writeByte(decByte);
			} catch (EOFException end) {
				break;
			}
		}
		progress = file.length();
		reader.close();tmpWriter.close();
		reader = null;
		tmpWriter = null;
		
		//copy data back
		writer = new DataOutputStream(new FileOutputStream(file));
		tmpReader = new DataInputStream(new FileInputStream(tmp));
		byte b;
		while (true) {
			try {
				b = tmpReader.readByte();
				writer.writeByte(b);
				
				progress++;
				if (progressKept[(int)(progress * 100 / bytesToTransfer)] == 0) {
					progressKept[(int)(progress * 100 / bytesToTransfer)] = 1;
					updateProgress((int)(progress * 100 / bytesToTransfer));
				}
			} catch (EOFException end) {
				break;
			}
		}
		updateProgress(100);
		writer.close();tmpReader.close();
		writer = null;tmpReader = null;
		
		//delete tmp
		tmp.delete();
		tmp = null;
		
		file = null;
	}
	
	public void addListener(EncryptionStatusListener listener) {
		//add another listener to list
		EncryptionStatusListener[] temp = new EncryptionStatusListener[listeners.length + 1];
		for ( int i=0 ; i<listeners.length ; i++ ) {
			temp[i] = listeners[i];
		}
		temp[listeners.length] = listener;
		listeners = temp;
	}
	
	public boolean removeListener(EncryptionStatusListener listener) {
		//returns true if removed
		EncryptionStatusListener[] temp = new EncryptionStatusListener[listeners.length - 1];
		boolean found = false;
		for ( int i=0 ; i<listeners.length ; i++ ) {
			if (listeners[i] == listener && !found) {
				found = true;
				continue;
			} else {
				if (found) {
					temp[i - 1] = listeners[i];
				} else {
					temp[i] = listeners[i];
				}
			}
		}
		listeners = temp;
		return found;
	}
	
	//events
	private void locationChanged(String path) {
		new Thread(new Runnable() {
			public void run() {
				for ( int i=0 ; i<listeners.length ; i++ ) {
					listeners[i].locationChanged(path);
				}
			}
		}).start();
	}
	
	private void updateProgress(int progress) {
		new Thread(new Runnable() {
			public void run() {
				for ( int i=0 ; i<listeners.length ; i++ ) {
					listeners[i].updateProgress(progress);
				}
			}
		}).start();
	}
}