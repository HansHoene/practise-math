package hansedwardhoene.cryptography;

import java.math.BigInteger;

public class CryptographyByteManager {
	public final static int BASE_TEN = 10;
	public final static int BINARY = 2;
	public final static int OCTAL = 8;
	public final static int HEXADECIMAL = 16;
	
	public final static char[] BIG_BASE_DIGITS = {'A','B','C','D','E','F'};
	
	private int base;
	private int publicKey;
	private int privateKey;
	private BigInteger modulus;
	
	private int numberLength;
	
	public CryptographyByteManager(int base, int publicKey, int privateKey, int modulus, boolean checkKeys) {
		//validate input
		if (publicKey <= 1 || privateKey <= 1) {
			throw new IllegalArgumentException("Key values may not be less than two.");
		}
		if (base != BINARY && base != OCTAL && base != HEXADECIMAL && base != BASE_TEN) {
			throw new IllegalArgumentException("The base may only be 2, 8, 16, or 10 to represent numbers in base ten, binary, octal, and hexadecimal.");
		}
		if (modulus <= (Math.abs(Byte.MIN_VALUE) + Math.abs(Byte.MAX_VALUE))) {
			throw new IllegalArgumentException("Modulus must be greater than "+(Math.abs(Byte.MIN_VALUE) + Math.abs(Byte.MAX_VALUE))+" to support every byte value.");
		}
		
		this.base = base;
		this.publicKey = publicKey;
		this.privateKey = privateKey;
		this.modulus = BigInteger.valueOf(modulus);
		
		this.numberLength = 0;
		
		//find maximum number length necessary
		{
			//length is number length (amount of digits needed to fully represent modulus)
			for ( int length=1 ; true ; length++ ) {
				BigInteger sum = BigInteger.ZERO; //count sum of each base multiplied by (base - 1) ^ pos
				for ( int i=0 ; i<length ; i++ ) {
					sum = sum.add(BigInteger.valueOf(this.base - 1).multiply(BigInteger.valueOf(this.base).pow(i)));
				}
				if (sum.compareTo(this.modulus) >= 0) {
					sum = null;
					this.numberLength = length;
					break;
				}
				sum = null;
			}
		}
		
		if (this.numberLength == 0) {
			throw new InternalError("Unknown error ocurred");
		}
		
		//check if keys work
		if (checkKeys) {
			for ( int i=Byte.MIN_VALUE ; i<=Byte.MAX_VALUE ; i++ ) {
				//System.out.print();
				if (BigInteger.valueOf(i).compareTo(BigInteger.valueOf(i + Math.abs(Byte.MIN_VALUE)).pow(this.publicKey).mod(this.modulus).pow(this.privateKey).mod(this.modulus).subtract(BigInteger.valueOf(Math.abs(Byte.MIN_VALUE)))) != 0) {
					throw new IllegalArgumentException("Key values do not work.");
				}
			}
		}
	}
	
	public String encrypt(byte x) {
		BigInteger encByte = BigInteger.valueOf(x + Math.abs(Byte.MIN_VALUE)).pow(publicKey).mod(modulus); //encrypt value
		String baseString = "";
		for ( int pos=(numberLength - 1) ; pos >=0 ; pos-- ) { //loop through positions
			for ( int value=(base - 1) ; value >= 0 ; value-- ) { //loop through every possible value of num in position until it finds appropriate value
				BigInteger test = BigInteger.valueOf(value).multiply(BigInteger.valueOf(base).pow(pos)); //value of num * (base ^ pos)
				if (test.compareTo(encByte) <= 0) {
					encByte = encByte.subtract(test);
					if (value <= 9) {
						baseString += value;
					} else if (value <= 16) {
						baseString += BIG_BASE_DIGITS[value - 10];
					}
					break;
				}
			}
		}
		return baseString;
	}
	
	public String encrypt(byte[] x) {
		String ret = "";
		for ( int i=0 ; i<x.length ; i++ ) {
			ret += encrypt(x[i]);
		}
		return ret;
	}
	
	public byte[] decrypt(byte[] b) {
		//ret is return array, temp is temporary used to add bytes to temp,
				byte[] ret = new byte[0], temp = null;
				char nextChar;int charToNum, sum;byte decByte;//nextChar-next character in string,charToNum-character converted to its number format (or possibly letter to number if hexadecimal), decByte-decrypted byte
				for ( int i=0 ; i<b.length ; i+=numberLength ) { //loop through every byte group
					sum = 0;
					for ( int c=0 ; c<numberLength ; c++ ) {//loop through characer in group and add to sum
						//read byte long enough for number
						nextChar = (char)(b[c+i] & 0xFF);
						try {
							charToNum = Integer.parseInt(String.valueOf(nextChar));
						} catch (NumberFormatException err) {
							charToNum = 0;
							for ( int loop=0 ; loop<BIG_BASE_DIGITS.length ; loop++ ) {
								if (nextChar == BIG_BASE_DIGITS[loop]) {
									charToNum = loop + 10;
									break;
								}
							}
						}
						if (charToNum >= base) {
							throw new IllegalArgumentException("Illegal characters in text to decrypt.");
						}
						sum += (charToNum * Math.pow(base, numberLength - c - 1));
					}
					decByte = BigInteger.valueOf(sum).pow(privateKey).mod(modulus).subtract(BigInteger.valueOf(Math.abs(Byte.MIN_VALUE))).byteValue();
					temp = new byte[ret.length + 1];
					for ( int k=0 ; k<ret.length ; k++ ) {
						temp[k] = ret[k];
					}
					temp[ret.length] = decByte;
					ret = temp;
					temp = null;
				}
				temp = null;
				b = null;
				return ret;
	}
	
	public byte[] decrypt(String s) {
		return decrypt(s.getBytes());
	}
	
	public int getNumberLength() {
		return this.numberLength;
	}

}