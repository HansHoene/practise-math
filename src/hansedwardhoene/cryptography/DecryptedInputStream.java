package hansedwardhoene.cryptography;

import java.io.*;

public class DecryptedInputStream extends InputStream implements Closeable {
	
	private DataInputStream in;
	private CryptographyByteManager worker;
	private byte[] temp;
	
	public DecryptedInputStream(InputStream in, CryptographyByteManager worker) {
		this.in = new DataInputStream(in);
		in.mark(0);
		this.worker = worker;
		temp = new byte[0];
	}
	
	@Override //method so that this is an official inputstream
	public int read() throws IOException {
		try {
			return (int)nextByte() & 0x000000FF;
		} catch (EOFException e) {
			return -1;
		}
	}
	
	public byte nextByte() throws EOFException, IOException {
		temp = new byte[worker.getNumberLength()];
		for ( int i=0 ; i<worker.getNumberLength() ; i++ ) {
			temp[i] = in.readByte();
		}
		return worker.decrypt(temp)[0];
	}
	
	public char nextChar() throws EOFException, IOException {
		temp = new byte[worker.getNumberLength()];
		for ( int i=0 ; i<worker.getNumberLength() ; i++ ) {
			temp[i] = in.readByte();
		}
		return (char)(worker.decrypt(temp)[0] & 0xFF);
	}
	
	public String nextLine() throws EOFException, IOException {
		char c;
		try {
			c = nextChar();
		} catch (EOFException e) {
			return null;
		}
		if (c == '\n') return "";
		String ret = ""+c;
		while (true) {
			try {
				c = nextChar();
			} catch (EOFException e) {
				break; //breaks if last line
			}
			if (c == '\n') break;
			else ret += nextChar();
		}
		return ret;
	}
	
	public void close() throws IOException {
		in.close();
	}
}