// Program: EncryptionStatusListener.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Feb 28, 2016        	 HEH       				Created

package hansedwardhoene.cryptography;

/*filename: EncryptionStatusListener.java*/
public interface EncryptionStatusListener {
	
	public void updateProgress(int progress); //percent progress of current file
	
	public void locationChanged(String path); //event executes when location in file system changes
}