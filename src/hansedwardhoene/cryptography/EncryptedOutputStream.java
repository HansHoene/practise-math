package hansedwardhoene.cryptography;

import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;

public class EncryptedOutputStream extends OutputStream implements Closeable, Flushable {
	
	private DataOutputStream out;
	private CryptographyByteManager worker;
	
	public EncryptedOutputStream(OutputStream out, CryptographyByteManager worker) {
		this.out = new DataOutputStream(out);
		this.worker = worker;
	}
	
	@Override //so that this is an official outputstream
	public void write(int arg0) throws IOException {
		write(String.valueOf((char)arg0).getBytes());
	}
	
	public void write(String x) throws IOException {
		out.writeBytes(worker.encrypt(x.getBytes()));
	}
	
	public void write(byte b) throws IOException {
		out.writeBytes(worker.encrypt(b));
	}
	
	public void writeLine(String x) throws IOException {
		out.writeBytes(worker.encrypt((x+"\n").getBytes()));
	}
	
	public void write(byte[] b) throws IOException {
		out.writeBytes(worker.encrypt(b));
	}
	
	public void flush() throws IOException {
		out.flush();
	}
	
	public void close() throws IOException {
		out.close();
	}
}