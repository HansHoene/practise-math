// Program: AlphabeticalOrder.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Mar 23, 2016        	 HEH       				Created

package hansedwardhoene;

/*filename: AlphabeticalOrder.java*/
public class AlphabeticalOrder implements ArraySorter<String> {
	@Override
	public boolean comesBefore(String before, String after) {
		//
		char[] uppercase = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
		char[] lowercase = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
		int charAt = 0;
		while (before.charAt(charAt) == after.charAt(charAt) && charAt < (before.length() - 1) && charAt < (after.length() - 1)) {
			charAt++;
		}
		if (before.charAt(charAt) == after.charAt(charAt)) {
			return before.length() < after.length();
		}
		for ( int i=0 ; i<uppercase.length ; i++ ) {
			if (before.charAt(charAt) == uppercase[i] || before.charAt(charAt) == lowercase[i]) {
				return true;
			} else if (after.charAt(charAt) == uppercase[i] || after.charAt(charAt) == lowercase[i]) {
				return false;
			}
		}
		return false;
	}
}