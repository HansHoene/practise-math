// Program: t.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Mar 9, 2016        	 HEH       				Created

package hansedwardhoene;

/*filename: t.java*/
public class t2 {

	public static void main(String[] args) {
		//
		
		
		Progress p = new Progress();
		p.next();
		p.next();
		System.out.println(p.getAverageTime());
		(new Thread() {
			public void run() {
				while (true) {try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				p.next();
				System.out.println("Progress: "+p.getProgress()+"\nAVG Time: "+p.getAverageTime()+"\n");}
			}
		}).start();
		/*System.exit(0);
		Array<String> x = new Array<String>();
		x.add("hello","Antarctica","New Zealand","Hans","Chris","Kyle","Computer","Yemen","Bob","Bobby", "23", "12");
		//System.out.print(x);
		x.sort(new AlphabeticalOrder());
		System.out.print(x);*/
		
	}

}