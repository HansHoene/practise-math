package hansedwardhoene;

public class Progress {
	
	private long timer;
	private long[] timerTracker;
	
	public Progress() {
		timer = System.currentTimeMillis();
		timerTracker = new long[0];
	}
	
	public void next() {
		long[] temp = new long[timerTracker.length + 1];
		for ( int i=0 ; i<timerTracker.length ; i++ ) {
			temp[i] = timerTracker[i];
		}
		temp[timerTracker.length] = -timer + (timer = System.currentTimeMillis());
		timerTracker = temp;
		temp = null;
	}
	
	public double getAverageTime() {
		double sum = 0;
		for ( int i=0 ; i<timerTracker.length ; i++ ) {
			sum += timerTracker[i];
		}
		return sum / (double)timerTracker.length;
	}
	
	public int getProgress() {
		return timerTracker.length;
	}
}