// Program: ArraySorter.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Mar 21, 2016        	 HEH       				Created

package hansedwardhoene;

/*filename: ArraySorter.java*/
public interface ArraySorter<T> {
	public boolean comesBefore(T before, T after);
}