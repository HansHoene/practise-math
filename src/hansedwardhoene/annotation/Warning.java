// Program: Warning.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Feb 27, 2016        	 HEH       				Created

package hansedwardhoene.annotation;

import java.lang.annotation.*;

/*filename: Warning.java*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only.
public @interface Warning {
	public String value();
}