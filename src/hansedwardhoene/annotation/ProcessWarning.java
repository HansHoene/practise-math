// Program: ProcessWarning.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Feb 27, 2016        	 HEH       				Created

package hansedwardhoene.annotation;

import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;

/*filename: ProcessWarning.java*/
public class ProcessWarning extends javax.annotation.processing.AbstractProcessor {

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		processingEnv.getMessager().printMessage(javax.tools.Diagnostic.Kind.WARNING, "hi");
		return true;
	}
	
}