package hansedwardhoene.database;

public class DatabaseFormatException extends Throwable {

	private static final long serialVersionUID = 1L;

	public DatabaseFormatException(String message) {
		super(message);
	}

	public DatabaseFormatException() {
		super();
	}
}
