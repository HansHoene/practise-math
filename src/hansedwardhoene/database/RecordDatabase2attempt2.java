package hansedwardhoene.database;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

public class RecordDatabase2attempt2 implements Iterable<String[]>, Iterator<String[]>, Closeable, AutoCloseable {
	
	private InputStream in;
	private OutputStream out;
	
	private StreamOpener so;
	
	private long location;
	
	private String[] next;
	
	/*
	 * Simultaneous input and ouput
	 * track location
	 * write to temp file
	 * write to provided outputstream
	 * create deleting feature?
	 * */
	
	RecordDatabase2attempt2(StreamOpener opener) throws IOException {
		in = null;out = null;so = opener;
		in = so.openInputStream();
		location = 0;
		setNext();
	}
	
	public void writeRecord(String...record) throws IOException {
		in.close();
		out = so.openOutputStream();
		out.write("\n".getBytes());
		for ( int i=0 ; i<record.length ; i++ ) {
			out.write((record[i].replaceAll("\t", "")+(i == record.length - 1 ? "" : "\t")).getBytes());
		}
		out.close();
		out = null;
		in = so.openInputStream();
		in.skip(location);
	}

	@Override
	public void close() throws IOException {
		if (in != null) in.close();
		if (out != null) out.close();
	}

	@Override
	public boolean hasNext() {
		return next != null;
	}

	@Override
	public String[] next() {
		String[] r = next;
		try {
			setNext();
		} catch (IOException e) {
			next = null;
		}
		return r;
	}

	@Override
	public Iterator<String[]> iterator() {
		return this;
	}
	
	private void setNext() throws IOException {
		int n = in.read();
		char c = (char)(n & 0xFF);
		if (n == -1) {
			next = null;
			return;
		} else if (c == '\n') {
			location++;
			setNext();
			return;
		}
		String temp = "";
		while (n != -1 && c != '\n') {
			location++;
			temp += c;
			n = in.read();
			c = (char)(n & 0xFF);
		}
		next = temp.split("\t");
	}
}