package hansedwardhoene.database;

public interface StatusListener {
	
	public void updateProgress(double p);
	
}