package hansedwardhoene.database;

import java.io.InputStream;
import java.io.OutputStream;

public interface StreamOpener {
	
	public InputStream openInputStream();
	
	public OutputStream openOutputStream();
	
}