package hansedwardhoene.database;

public abstract class Record {
	
	protected int columns;
	
	private String[] headers;
	
	public abstract Record newRecord();
	
	public Record() {
		this.headers = null;
		this.columns = 0;
	}
	
	protected void setHeaders(String...headers) {
		this.headers = new String[headers.length];
		for ( int i=0 ; i<headers.length ; i++ ) {
			this.headers[i] = headers[i].replaceAll("\t", "");
		}
		this.columns = headers.length;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public String[] getHeaders() {
		String[] ret = new String[headers.length];
		for ( int i=0 ; i<headers.length ; i++ ) {
			ret[i] = ""+headers[i];
		}
		return ret;
	}
	
	public abstract String[] getValues();
	
	public abstract void setValues(String...vals);
	
}