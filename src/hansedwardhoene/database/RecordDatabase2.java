package hansedwardhoene.database;

import hansedwardhoene.cryptography.CryptographyByteManager;
import hansedwardhoene.cryptography.DecryptedInputStream;
import hansedwardhoene.cryptography.EncryptedOutputStream;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Random;

public class RecordDatabase2 implements Closeable, AutoCloseable,
		Iterable<String[]>, Iterator<String[]> {
			
	private String[] nextArr;
	private File tmp;
	private OutputStream out;
	private DecryptedInputStream tIn;
	private EncryptedOutputStream tOut;
	private final CryptographyByteManager WORKER;
	private String[] headers;
	private long length;
	private long location;
	
	private static Random random = new Random();
	private static StatusListener listener = null;
	
	private static final int[][] CRYPTOGRAPHY_INTS = {
		{5, 53, 299},
	};
	
	public static StatusListener getListener() {
		return listener;
	}

	public static void setListener(StatusListener listener) {
		RecordDatabase2.listener = listener;
	}
	
	public static String generateCode() {
		String r = "";
		int i;
		for ( i=0 ; i<3 ; i++ ) {
			r += (char) (random.nextInt(26) + 'A');
		}
		for ( i=0 ; i<3 ; i++ ) {
			r += random.nextInt(10);
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += (char) (random.nextInt(26) + 'A');
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += random.nextInt(10);
		}
		return r;
	}
	
	public static RecordDatabase2 loadDatabase(InputStream data, OutputStream result) throws IOException, DatabaseFormatException {
		return new RecordDatabase2(data, result, null);
	}
	
	public static RecordDatabase2 newDatabase(OutputStream result, String...headers) throws IOException, DatabaseFormatException {
		return new RecordDatabase2(null, result, headers);
	}
	
	private RecordDatabase2(InputStream data, OutputStream result, String[] headers) 
			throws IOException, DatabaseFormatException {
		int[] keys = CRYPTOGRAPHY_INTS[random.nextInt(CRYPTOGRAPHY_INTS.length)];
		this.WORKER = new CryptographyByteManager(
				random.nextBoolean() ? (random.nextBoolean() ? CryptographyByteManager.BINARY : CryptographyByteManager.OCTAL) : (random.nextBoolean() ? CryptographyByteManager.BASE_TEN : CryptographyByteManager.HEXADECIMAL),
						keys[0],
						keys[1],
						keys[2],
						true);
		new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/").mkdirs();
		do {
			this.tmp = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/"+generateCode()+".tmp");
		} while (tmp.exists());
		this.tmp.createNewFile();
		
		if (data != null) {
			this.tOut = new EncryptedOutputStream(new FileOutputStream(tmp), WORKER);
			long line = 0, count = 0;
			int next = data.read();
			char c = (char)(next & 0xFF), prevChar = '\t';
			String lineString = "";
			if (next == -1) throw new DatabaseFormatException("No Data Available.");
			while (next != -1) {
				
				if (c == '\n') {
					if (line++ == 0) {
						//adds one to line
						//if first line, set headers
						this.headers = lineString.split("\t");
						if (this.headers.length == 0)
							throw new DatabaseFormatException("No headers");
					} else {
						//check headers if not first line
						if (lineString.split("\t").length != this.headers.length)
							throw new DatabaseFormatException("Columns do not match number of headers in line#"+(line - 1));
						
						tOut.write("\r\n".getBytes());
						//System.out.write("\r\n".getBytes());
					}
					lineString = "";//reset line
				}
				
				if (listener != null && count++ % 100 == 0) {
					listener.updateProgress(count);
				}
				
				if (line > 0 && c != '\n' && c != '\r') {
					tOut.write(next);//only write data, not headers
					//System.out.write(next);
				}
				
				lineString += (c == '\r' || c == '\n' ? "" : ""+c);
				
				prevChar = c;
				next = data.read();
				c = (char)(next & 0xFF);
			}
			if (prevChar != '\n') {
				tOut.write("\r\n".getBytes());
				result.write("\r\n".getBytes());
			}
			this.length = line - 1;
			this.tOut.close();
			this.tOut = null;
		} else {
			this.headers = headers;
			if (headers.length == 0)
				throw new DatabaseFormatException("At least one column is required");
			
			for ( int i=0 ; i<headers.length ; i++ ) {
				result.write((headers[i].replaceAll("\t", "")+(i == headers.length - 1 ? "" : "\t")).getBytes());
			}
			result.write("\r\n".getBytes());
			this.length = 0;
		}
		
		this.out = result;
		this.tIn = new DecryptedInputStream(new FileInputStream(tmp), WORKER);
		this.location = 0;
		setNext();
	}
	
	private void setNext() {
		//sets up nextArr
		try {
			int next = this.tIn.read();
			location++;
			char c = (char)(next & 0xFF);
			
			if (next == -1) {
				nextArr = null;
				return;
			} else if (c == '\n') {
				setNext();
				return;
			}
			
			String val = "";
			
			while (next != -1 && c != '\n') {
				val += (c == '\n' || c == '\r' ? "" : ""+c);
				next = this.tIn.read();
				location++;
				c = (char)(next & 0xFF);
			}
			
			nextArr = val.split("\t");
		} catch (IOException e) {
			nextArr = null;
		}
	}
	
	public void addRecord(String...r) throws DatabaseFormatException, IOException {
		if (r.length != this.headers.length)
			throw new DatabaseFormatException("Record must be the same length as the number of headers: "+this.headers.length);
		tIn.close();
		tIn = null;
		
		tOut = new EncryptedOutputStream(new FileOutputStream(tmp, true), WORKER);
		for ( int i=0 ; i<r.length ; i++ ) {
			out.write((r[i]+(i == r.length - 1 ? "" : "\t")).getBytes());
			tOut.write((r[i]+(i == r.length - 1 ? "" : "\t")).getBytes());
		}
		out.write("\r\n".getBytes());
		tOut.write("\r\n".getBytes());
		tOut.close();
		tOut = null;
		
		tIn = new DecryptedInputStream(new FileInputStream(tmp), WORKER);
		tIn.skip(location);
	}
	
	public void reset() {
		try { tIn.close(); } catch (Throwable e) {}
		try {
			tIn = new DecryptedInputStream(new FileInputStream(tmp), WORKER);
		} catch (FileNotFoundException e) {
			throw new InternalError();
		}
		location = 0;
		setNext();
	}
	
	public long length() {
		return length;
	}
	
	public int getNumberOfColumns() {
		return headers.length;
	}
	
	public String[] getHeaders() {
		String[] r = new String[headers.length];
		for ( int i=0 ; i<headers.length ; i++ ) {
			r[i] = headers[i]+"";
		}
		return r;
	}
	
	/*public void editRecord(String[] record, String...vals) throws DatabaseFormatException, IOException {
		if (record.length != headers.length || vals.length != headers.length) {
			throw new DatabaseFormatException("Illegal Number of vallues.");
		}
		File temp;
		do {
			temp = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/"+generateCode()+".tmp");
		} while (temp.exists());
		temp.createNewFile();
		
		String[] line;
		int i;
		reset();
		EncryptedOutputStream eos = new EncryptedOutputStream(new FileOutputStream(temp), WORKER);
		line = getHeaders();
		for ( i=0 ; i<line.length ; i++ ) {
			eos.write(line[i].getBytes());
			if (i!= line.length - 1) eos.write("\t".getBytes());
		}
		eos.write("\r\n".getBytes());
		
		outer: while (hasNext()) {
			line = next();
			for ( i=0 ; i<line.length ; i++ ) {
				if (!line[i].equals(record[i])) {
					//write every record because there is no match
					for ( i=0 ; i<line.length ; i++ ) {
						eos.write(line[i].getBytes());
						if (i!= line.length - 1) eos.write("\t".getBytes());
					}
					continue outer;
				}
			}
			//everything matched, so fix record
			for ( i=0 ; i<vals.length ; i++ ) {
				eos.write(vals[i].getBytes());
				if (i!= vals.length - 1) eos.write("\t".getBytes());
			}
		}
		eos.close();eos = null;
		try { this.tIn.close(); } catch (IOException e) {}
		try { this.tOut.close(); } catch (IOException e) {}
		
		DecryptedInputStream dos = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		eos = new EncryptedOutputStream(new FileOutputStream(tmp, false), WORKER);
		i = dos.read();
		while (i != -1) {
			eos.write(i);
			i = dos.read();
		}
		eos.close();
		dos.close();
		reset();
	}*/

	@Override
	public boolean hasNext() {
		return nextArr != null;
	}

	@Override
	public String[] next() {
		String[] ret = nextArr;
		setNext();
		return ret;
	}

	@Override
	public Iterator<String[]> iterator() {
		return this;
	}
	
	@Override
	public void close() throws IOException {
		IOException e = null;
		try { tOut.close(); } catch (Throwable e2) {}
		try { tIn.close(); } catch (IOException e1) {e = e1;} catch (Throwable e2) {}
		try { out.close(); } catch (IOException e1) {e = e1;} catch (Throwable e2) {}
		tmp.delete();
		if (e != null) throw e;
	}
	
	/*public static void main(String[] args) throws FileNotFoundException, IOException, DatabaseFormatException {
		File x = new File("C:/userS/HANSH/dESKTOP/my_test.txt");
		RecordDatabase d = RecordDatabase.loadDatabase(new FileInputStream(x),
				new FileOutputStream(x, true));
		for ( int i=0 ; i<d.getHeaders().length ; i++ ) {
			System.out.print(d.getHeaders()[i]+"\t");
		}
		System.out.println();
		for ( int i=0 ; i<d.getHeaders().length ; i++ ) {
			System.out.print("----\t");
		}
		System.out.println();
		
		while (d.hasNext()) {
			String[] a = d.next();
			for ( int i=0 ; i<a.length ; i++ ) {
				System.out.print(a[i]+"\t");
			}
			System.out.println();
		}
		d.close();
	}*/
}
