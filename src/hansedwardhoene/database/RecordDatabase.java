package hansedwardhoene.database;

import hansedwardhoene.cryptography.CryptographyByteManager;
import hansedwardhoene.cryptography.DecryptedInputStream;
import hansedwardhoene.cryptography.EncryptedOutputStream;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Random;

public class RecordDatabase implements Closeable, AutoCloseable,
		Iterable<String[]>, Iterator<String[]> {
			
	private String[] nextArr;
	private File file;
	private OutputStream out;
	private InputStream in;
	private final CryptographyByteManager WORKER;
	private String[] headers;
	private long length;
	private long location;
	
	private static Random random = new Random();
	private static StatusListener listener = null;
	
	public static StatusListener getListener() {
		return listener;
	}

	public static void setListener(StatusListener listener) {
		RecordDatabase.listener = listener;
	}
	
	public static String generateCode() {
		String r = "";
		int i;
		for ( i=0 ; i<3 ; i++ ) {
			r += (char) (random.nextInt(26) + 'A');
		}
		for ( i=0 ; i<3 ; i++ ) {
			r += random.nextInt(10);
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += (char) (random.nextInt(26) + 'A');
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += random.nextInt(10);
		}
		return r;
	}
	
	public static RecordDatabase loadDatabase(File f) throws IOException, DatabaseFormatException {
		return new RecordDatabase(f, null, null);
	}
	
	public static RecordDatabase loadEncryptedDatabase(File f, CryptographyByteManager cbm) throws IOException, DatabaseFormatException {
		return new RecordDatabase(f, cbm, null);
	}
	
	public static RecordDatabase newDatabase(File f, String...headers) throws IOException, DatabaseFormatException {
		return new RecordDatabase(f, null, headers);
	}
	
	public static RecordDatabase newEncryptedDatabase(File f, CryptographyByteManager cbm, String...headers) throws IOException, DatabaseFormatException {
		return new RecordDatabase(f, cbm, headers);
	}
	
	private RecordDatabase(File f, CryptographyByteManager w, String[] headers) throws IOException, DatabaseFormatException {
		this.WORKER = w;
		
		if (headers == null) {
			//load
			in = WORKER == null ?
					new FileInputStream(f) :
						new DecryptedInputStream(new FileInputStream(f), WORKER);
			this.length = 0;
			this.location = 0;
			String line = "";
			int next = in.read();
			char c = (char)(next & 0xFF), prevChar = '\t';
			if (next == -1)
				throw new DatabaseFormatException("No data available.");
			
			long count = 0, fileLength = WORKER == null ? f.length() : f.length() / WORKER.getNumberLength();
			
			while (next != -1) {
				if (count++ % 100 == 0 && listener != null) listener.updateProgress(count * 100 / fileLength);
				if (c == '\n') {
					if (length++ == 0) {
						this.headers = line.split("\t");
					} else {
						if (line.split("\t").length != this.headers.length)
							throw new DatabaseFormatException("Discrepancies in row length.");
					}
					line = "";
				} else {
					if (c != '\r') line += c;
				}
				prevChar = c;
				next = in.read();
				c = (char)(next & 0xFF);
			}
			in.close();
			in = null;
			if (prevChar != '\n') {
				out = WORKER == null ?
						new FileOutputStream(f, true) :
							new EncryptedOutputStream(new FileOutputStream(f, true), WORKER);
				out.write("\r\n".getBytes());
				out.close();
				out = null;
				this.length++;
			}
		} else {
			//create
			this.headers = headers;
			if (headers.length == 0)
				throw new DatabaseFormatException("At least one header required!");
			out = WORKER == null ?
					new FileOutputStream(f, false) :
						new EncryptedOutputStream(new FileOutputStream(f, false), WORKER);
			for ( int i=0 ; i<headers.length ; i++ ) {
				out.write(headers[i].getBytes());
				if (i != headers.length - 1) out.write("\t".getBytes());
			}
			out.write("\r\n".getBytes());
			out.close();
			out = null;
			this.length = 0;
			this.location = 0;
		}
		
		in = WORKER == null ?
				new FileInputStream(f) :
					new DecryptedInputStream(new FileInputStream(f), WORKER);
				
		this.file = f;
		setNext();
		setNext();
	}
	
	private void setNext() {
		//sets up nextArr
		try {
			int next = in.read();
			location++;
			char c = (char)(next & 0xFF);
			
			if (next == -1) {
				nextArr = null;
				return;
			} else if (c == '\n') {
				setNext();
				return;
			}
			
			String val = "";
			
			while (next != -1 && c != '\n') {
				val += (c == '\n' || c == '\r' ? "" : ""+c);
				next = in.read();
				location++;
				c = (char)(next & 0xFF);
			}
			
			nextArr = val.split("\t");
		} catch (IOException e) {
			nextArr = null;
		}
	}
	
	public void addRecord(String...r) throws DatabaseFormatException, IOException {
		if (r.length != this.headers.length)
			throw new DatabaseFormatException("Record must be the same length as the number of headers: "+this.headers.length);
		in.close();
		in = null;
		
		out = WORKER == null ?
				new FileOutputStream(file, true) :
					new EncryptedOutputStream(new FileOutputStream(file, true), WORKER);
		for ( int i=0 ; i<r.length ; i++ ) {
			out.write((r[i]+(i == r.length - 1 ? "" : "\t")).getBytes());
		}
		out.write("\r\n".getBytes());
		out.close();
		out = null;
		
		in = WORKER == null ?
				new FileInputStream(file) :
					new DecryptedInputStream(new FileInputStream(file), WORKER);
		in.skip(location);
		
		length++;
	}
	
	public void reset() {
		try { in.close(); } catch (Throwable e) {}
		try {
			in = WORKER == null ?
					new FileInputStream(file) :
						new DecryptedInputStream(new FileInputStream(file), WORKER);
		} catch (FileNotFoundException e) {
			throw new InternalError();
		}
		location = 0;
		setNext();
		setNext();
	}
	
	public long length() {
		return length;
	}
	
	public int getNumberOfColumns() {
		return headers.length;
	}
	
	public String[] getHeaders() {
		String[] r = new String[headers.length];
		for ( int i=0 ; i<headers.length ; i++ ) {
			r[i] = headers[i]+"";
		}
		return r;
	}
	
	public void editRecord(String[] record, String...vals) throws DatabaseFormatException, IOException {
		if (record.length != headers.length || vals.length != headers.length) {
			throw new DatabaseFormatException("Illegal Number of values.");
		}
		
		//boolean found = false;
		
		File temp;
		do {
			temp = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/"+generateCode()+".tmp");
		} while (temp.exists());
		temp.createNewFile();
		
		String[] line;
		int i;
		reset();
		EncryptedOutputStream eos = new EncryptedOutputStream(new FileOutputStream(temp), WORKER);
		line = getHeaders();
		for ( i=0 ; i<line.length ; i++ ) {
			eos.write(line[i].getBytes());
			if (i!= line.length - 1) eos.write("\t".getBytes());
		}
		eos.write("\r\n".getBytes());
		
		outer: while (hasNext()) {
			line = next();
			for ( i=0 ; i<line.length ; i++ ) {
				if (!line[i].equals(record[i])) {
					//write every record because there is no match
					for ( i=0 ; i<line.length ; i++ ) {
						eos.write(line[i].getBytes());
						if (i!= line.length - 1) eos.write("\t".getBytes());
					}
					eos.write("\r\n".getBytes());
					continue outer;
				}
			}
			//everything matched, so fix record
			//found = true;
			for ( i=0 ; i<vals.length ; i++ ) {
				eos.write(vals[i].getBytes());
				if (i!= vals.length - 1) eos.write("\t".getBytes());
			}
			eos.write("\r\n".getBytes());
		}
		eos.close();eos = null;
		try { in.close(); } catch (Throwable e) {}
		try { out.close(); } catch (Throwable e) {}
		
		DecryptedInputStream dos = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		out = WORKER == null ?
				new FileOutputStream(file, true) :
					new EncryptedOutputStream(new FileOutputStream(file, false), WORKER);
		i = dos.read();
		while (i != -1) {
			out.write(i);
			i = dos.read();
		}
		out.close();out = null;
		dos.close();
		
		reset();
		
		//return found;
	}
	
	public void deleteRecord(String[] record) throws DatabaseFormatException, IOException {
		if (record.length != headers.length) {
			throw new DatabaseFormatException("Illegal Number of values.");
		}
		
		boolean found = false;
		
		File temp;
		do {
			temp = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/"+generateCode()+".tmp");
		} while (temp.exists());
		temp.createNewFile();
		
		String[] line;
		int i;
		reset();
		EncryptedOutputStream eos = new EncryptedOutputStream(new FileOutputStream(temp), WORKER);
		line = getHeaders();
		for ( i=0 ; i<line.length ; i++ ) {
			eos.write(line[i].getBytes());
			if (i!= line.length - 1) eos.write("\t".getBytes());
		}
		eos.write("\r\n".getBytes());
		
		outer: while (hasNext()) {
			line = next();
			for ( i=0 ; i<line.length ; i++ ) {
				if (!line[i].equals(record[i])) {
					//write every record because there is no match
					for ( i=0 ; i<line.length ; i++ ) {
						eos.write(line[i].getBytes());
						if (i!= line.length - 1) eos.write("\t".getBytes());
					}
					eos.write("\r\n".getBytes());
					continue outer;
				}
			}
			//everything matched, so do not add record record
			found = true;
		}
		eos.close();eos = null;
		try { in.close(); } catch (Throwable e) {}
		try { out.close(); } catch (Throwable e) {}
		
		DecryptedInputStream dos = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		out = WORKER == null ?
				new FileOutputStream(file, true) :
					new EncryptedOutputStream(new FileOutputStream(file, false), WORKER);
		i = dos.read();
		while (i != -1) {
			out.write(i);
			i = dos.read();
		}
		out.close();out = null;
		dos.close();
		
		reset();
		
		if (found) length--;
		
		//return found;
	}

	@Override
	public boolean hasNext() {
		return nextArr != null;
	}

	@Override
	public String[] next() {
		String[] ret = nextArr;
		setNext();
		return ret;
	}

	@Override
	public Iterator<String[]> iterator() {
		return this;
	}
	
	@Override
	public void close() throws IOException {
		IOException e = null;
		try { in.close(); } catch (IOException e1) {e = e1;} catch (Throwable e2) {}
		try { out.close(); } catch (IOException e1) {e = e1;} catch (Throwable e2) {}
		if (e != null) throw e;
	}
	
	/*public static void main(String[] args) throws FileNotFoundException, IOException, DatabaseFormatException {
		File x = new File("C:/userS/HANSH/dESKTOP/my_test.txt");
		RecordDatabase d = RecordDatabase.loadDatabase(new FileInputStream(x),
				new FileOutputStream(x, true));
		for ( int i=0 ; i<d.getHeaders().length ; i++ ) {
			System.out.print(d.getHeaders()[i]+"\t");
		}
		System.out.println();
		for ( int i=0 ; i<d.getHeaders().length ; i++ ) {
			System.out.print("----\t");
		}
		System.out.println();
		
		while (d.hasNext()) {
			String[] a = d.next();
			for ( int i=0 ; i<a.length ; i++ ) {
				System.out.print(a[i]+"\t");
			}
			System.out.println();
		}
		d.close();
	}*/
}
