package hansedwardhoene.database;

import hansedwardhoene.cryptography.CryptographyByteManager;
import hansedwardhoene.cryptography.DecryptedInputStream;
import hansedwardhoene.cryptography.EncryptedOutputStream;

import java.io.*;
import java.util.Iterator;

public class RecordDatabaseatmpt1 implements Iterable<String[]>, Iterator<String[]>, Closeable, AutoCloseable {
	
	private final static CryptographyByteManager WORKER = new CryptographyByteManager(16, 5, 53, 299, true);
	
	private int location;
	private int cols;
	private String[] headers;
	private long length;
	
	private File temp;
	
	private EncryptedOutputStream tempOut;
	private DecryptedInputStream tempIn;
	
	private String[] nextRecord;
	
	private static StatusListener listener;
	
	public static void setStatusListener(StatusListener l) {
		listener = l;
	}
	
	public static StatusListener getStatusListener() {
		return listener;
	}
	
	public static RecordDatabaseatmpt1 parse(InputStream in) throws IOException, DatabaseFormatException {
		return new RecordDatabaseatmpt1(in);
	}
	
	private RecordDatabaseatmpt1(InputStream in) throws IOException, DatabaseFormatException {
		//create temporary file
		new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/").mkdirs();
		do {
			temp = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/PracticeMath_Question_Database"+Math.random()+".tmp");
		} while (temp.exists());
		temp.createNewFile();
		
		length = 0;
		location = 0;
		
		
		if (in != null) {
			String t = "";
			tempOut = new EncryptedOutputStream(new FileOutputStream(temp), WORKER);
			long count = 0;
			int next = in.read();
			char c;
			while (next != -1) {
				tempOut.write(next);
				c = (char)(next & 0xFF);
				t += c;
				
				if (c == '\n') {
					if (length++ == 0) {
						//always adds 1, returns true if first row
						this.headers = t.split("\t");
						this.cols = this.headers.length;
					} else {
						//not first row
						if (t.split("\t").length != cols) throw new DatabaseFormatException();
					}
					t = "";
				}
				
				if (++count % 100 == 0 && listener != null) try {
					listener.updateProgress(count); 
				} catch (Throwable e) {}
				
				next = in.read();
			}
			tempOut.close();
		}
		tempOut = null;
		
		tempIn = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		
		setupNextRecord();
		setupNextRecord();//twice to skip headers
	}
	
	private void setupNextRecord() throws IOException {
		String temp = "";
		int next = tempIn.read();
		char c = (char)(next & 0xFF);
		if (next == -1) {
			nextRecord = null;
			return;
		} else if (c == '\n') {
			setupNextRecord();
			return;
		}
		while (next != -1 && c != '\n') {
			location++;
			temp += c;
			next = tempIn.read();
			c = (char)(next & 0xFF);
		}
		nextRecord = temp.split("\t");
	}
	
	public long length() {
		return length;
	}
	
	public void writeRecord(String...record) throws IOException, DatabaseFormatException {
		tempIn.close();
		tempIn = null;
		
		tempOut = new EncryptedOutputStream(new FileOutputStream(temp, true), WORKER);
		
		if (record.length < cols) throw new DatabaseFormatException();
		
		tempOut.write('\n');
		for ( int i=0 ; i<cols; i++ ) {
			if (i < record.length) {
				tempOut.write(record[i]);
			} else {
				tempOut.write("null".getBytes());
			}
			if (i != cols-1) {
				tempOut.write("\t");
			}
		}
		tempOut.close();
		tempOut = null;
		
		tempIn = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		tempIn.skip(location);
	}
	
	public String[] getHeaders() {
		String[] ret = new String[headers.length];
		for ( int i=0 ; i<headers.length ; i++ ) {
			ret[i] = ""+headers[i];
		}
		return ret;
	}
	
	public void reset() {
		try {
			tempIn.close();
		} catch (IOException e) {}
		tempIn = null;
		try {
			tempIn = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		} catch (FileNotFoundException e) {
			throw new InternalError();
		}
		location = 0;
		try {
			setupNextRecord();
			setupNextRecord();//twice to skip headers
		} catch (IOException e) {}
	}

	@Override
	public Iterator<String[]> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		return nextRecord != null;
	}

	@Override
	public String[] next() {
		String[] r = nextRecord;
		try {
			setupNextRecord();
		} catch (IOException e) {
			nextRecord = null;
		}
		return r;
	}
	
	public void load(OutputStream out) throws IOException {
		int x = location;
		try {
			tempIn.close();
		} catch (IOException e) {}
		tempIn = null;
		try {
			tempIn = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		} catch (FileNotFoundException e) {
			throw new InternalError();
		}
		int next = tempIn.read();
		while (next != -1) {
			out.write(next);
			next = tempIn.read();
		}
		out.close();
		try {
			tempIn.close();
		} catch (IOException e) {}
		tempIn = null;
		try {
			tempIn = new DecryptedInputStream(new FileInputStream(temp), WORKER);
		} catch (FileNotFoundException e) {
			throw new InternalError();
		}
		tempIn.skip(x);
		location = x;
	}

	@Override
	public void close() throws IOException {
		IOException err = null;
		try {
			tempIn.close();
			tempIn = null;
		} catch (IOException e) {
			err = e;
		}
		temp.delete();
		if (err != null) throw err;
	}
}