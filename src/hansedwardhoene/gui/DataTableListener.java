package hansedwardhoene.gui;

import java.awt.event.MouseEvent;

public interface DataTableListener {
	//Written by Hans-Edward Hoene
	
	public void cellClicked(MouseEvent e, int row, int col);
	
	public void cellValueChanged(int row, int col, String newVal);
}
