package hansedwardhoene.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.border.Border;

public class DataTable extends java.awt.ScrollPane {
	//Written by Hans-Edward Hoene

	private static final long serialVersionUID = 1L;
	
	private JLabel[][] data;
	private Panel pane;
	
	private int tableRows, tableCols;
	
	private Color back, fore;
	private Border border;
	
	private MouseAdapter event;
	
	private ArrayList<DataTableListener> listeners;
	
	public DataTable(int rows, int cols) {
		super();
		pane = new Panel();
		pane.setLayout(null);
		listeners = new ArrayList<DataTableListener>();
		tableRows = rows <= 0 ? 0 : rows;
		tableCols = cols <= 0 ? 0 : cols;
		event = (new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				Object target = e.getSource();
				int r, c;
				for ( r=0 ; r<tableRows ; r++ ) {
					for ( c=0 ; c<tableCols ; c++ ) {
						if (target == data[r][c]) {
							cellClicked(e, r, c);
						}
					}
				}
			}
		});
		data = new JLabel[tableRows][tableCols];
		for ( int r=0 ; r<tableRows ; r++ ) {
			for ( int c=0 ; c<tableCols ; c++ ) {
				data[r][c] = new JLabel();
				data[r][c].addMouseListener(event);
				data[r][c].setOpaque(true);
				pane.add(data[r][c]);
			}
		}
		setCellSize(10,10);
		setBackground(Color.white);
		setForeground(Color.black);
		setBorder(javax.swing.BorderFactory.createLineBorder(fore, 1));
		super.add(pane);
	}
	
	public void addListener(DataTableListener listener) {
		listeners.add(listener);
	}
	
	public boolean removeListener(DataTableListener listener) {
		return listeners.remove(listener);
	}
	
	public void removeListener(int index) {
		listeners.remove(index);
	}
	
	public void removeAllListeners() {
		listeners.clear();
	}
	
	public void addRow() {
		JLabel[][] copy = new JLabel[tableRows + 1][tableCols];
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				copy[r][c] = data[r][c];
			}
		}
		r = tableRows++;//last row then add one
		for ( c=0 ; c<tableCols ; c++ ) {
			copy[r][c] = new JLabel();
			copy[r][c].addMouseListener(event);
			copy[r][c].setOpaque(true);
			pane.add(copy[r][c]);
		}
		data = copy;
		copy = null;
		setBackground(this.back);
		setForeground(this.fore);
		setBorder(this.border);
		super.revalidate();
		super.repaint();
	}
	
	public void addCol() {
		JLabel[][] copy = new JLabel[tableRows][tableCols + 1];
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				copy[r][c] = data[r][c];
			}
		}
		c = tableCols++;//last col and add one
		for ( r=0 ; r<tableRows ; r++ ) {
			copy[r][c] = new JLabel();
			copy[r][c].addMouseListener(event);
			copy[r][c].setOpaque(true);
			pane.add(copy[r][c]);
		}
		data = copy;
		copy = null;
		setBackground(this.back);
		setForeground(this.fore);
		setBorder(this.border);
		super.revalidate();
		super.repaint();
	}
	
	public void removeRow(int row) {
		if (row < 0 || row >= data.length) throw new ArrayIndexOutOfBoundsException();
		JLabel[][] copy = new JLabel[--tableRows][tableCols];
		super.removeAll();
		int r, c;
		for ( r=0 ; r<(tableRows + 1) ; r++ ) {
			if (r == row) continue;
			for ( c=0 ; c<tableCols ; c++ ) {
				copy[r > row ? r - 1 : r][c] = data[r][c];
				pane.add(copy[r > row ? r - 1 : r][c]);
			}
		}
		data = copy;
		copy = null;
		setBackground(this.back);
		setForeground(this.fore);
		setBorder(this.border);
		super.revalidate();
		super.repaint();
	}
	
	public void removeCol(int col) {
		if (col < 0 || col >= data[0].length) throw new ArrayIndexOutOfBoundsException();
		JLabel[][] copy = new JLabel[tableRows][--tableCols];
		pane.removeAll();
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<(tableCols + 1) ; c++ ) {
				if (c == col) continue;
				copy[r][c > col ? c - 1 : c] = data[r][c];
				pane.add(copy[r][c > col ? c - 1 : c]);
			}
		}
		data = copy;
		copy = null;
		setBackground(this.back);
		setForeground(this.fore);
		setBorder(this.border);
		super.revalidate();
		super.repaint();
	}
	
	public void setValue(int row, int col, String value) {
		if (row < 1) throw new ArrayIndexOutOfBoundsException();
		data[row][col - 1].setText(value);
		cellValueChanged(row, col, value);
	}
	
	public String getValue(int row, int col) {
		if (row < 1) throw new ArrayIndexOutOfBoundsException();
		return data[row][col - 1].getText();
	}
	
	public void clearAllCells() {
		int r, c;
		for ( r=1 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				setValue(r, c, "");
			}
		}
	}
	
	public int getRows() {
		return tableRows - 1;
	}
	
	public int getCols() {
		return tableCols;
	}
	
	public void setCellSize(int colSize, int rowSize) {
		try {
			pane.setSize(tableCols * colSize, tableRows * rowSize);
		} catch (ArrayIndexOutOfBoundsException e) {}
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				data[r][c].setSize(colSize, rowSize);
				data[r][c].setLocation(c * colSize, r * rowSize);
			}
		}
	}
	
	public void setCellSize(Dimension d) {
		setCellSize(d.width, d.height);
	}
	
	public void setBorder(Border b) {
		this.border = b;
		//super.setBorder(this.border);
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				data[r][c].setBorder(this.border);
			}
		}
	}
	
	public void setBorder(int row, int col, Border border) {
		data[row][col].setBorder(border);
	}
	
	@Override
	public void setBackground(Color color) {
		this.back = color;
		super.setBackground(this.back);
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				data[r][c].setBackground(this.back);
			}
		}
	}
	
	public void setBackground(int row, int col, Color color) {
		data[row][col].setBackground(color);
	}
	
	@Override
	public void setForeground(Color color) {
		this.fore = color;
		super.setForeground(this.fore);
		int r, c;
		for ( r=0 ; r<tableRows ; r++ ) {
			for ( c=0 ; c<tableCols ; c++ ) {
				data[r][c].setForeground(this.fore);
			}
		}
	}
	
	public void setForeground(int row, int col, Color color) {
		data[row][col].setForeground(color);
	}
	
	/*@Override
	public void setBounds(int x, int y, int width, int height) {
		//super.setLocation(x, y);
		//setSize(width, height);
	}*/
	
	private void cellValueChanged(int row, int col, String newVal) {
		for ( int i=0 ; i<listeners.size() ; i++ ) {
			listeners.get(i).cellValueChanged(row, col, newVal);
		}
	}
	
	private void cellClicked(MouseEvent e, int row, int col) {
		for ( int i=0 ; i<listeners.size() ; i++ ) {
			listeners.get(i).cellClicked(e, row, col);
		}
	}
}