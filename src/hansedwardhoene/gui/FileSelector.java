package hansedwardhoene.gui;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FileSelector extends java.awt.Dialog {
	//Written by Hans-Edward Hoene
	
	private static final long serialVersionUID = 1L;
	
	private Label titleLabel, pathLabel;
	private TextField input;
	private Checkbox createNew;
	private Button submit, cancel, select;
	
	private MouseAdapter click;
	
	private final static Color BACKGROUND_COLOR = Color.lightGray, FOREGROUND_COLOR = Color.black;
	private final static Font TITLE_FONT = new Font("Times New Roman", 0, 36), REGULAR_FONT = new Font("Times New Roman", 0, 18);
	
	private String extension;
	
	private File value;
	
	private FileSelector(Frame parent, String title, String extension, boolean defaultCreateNewFile, boolean defaultIsChangeable) {
		super(parent, title, true);
		super.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				value = null;
				dispose();
			}
		});
		super.setLayout(null);
		super.setFont(REGULAR_FONT);
		super.setBackground(BACKGROUND_COLOR);
		super.setForeground(FOREGROUND_COLOR);
		
		this.extension = extension;
		this.value = null;
		
		titleLabel = new Label(title);
		titleLabel.setFont(TITLE_FONT);
		titleLabel.setForeground(FOREGROUND_COLOR);
		titleLabel.setBackground(BACKGROUND_COLOR);
		titleLabel.setLocation(10, 50);
		titleLabel.setSize(800, 50);
		add(titleLabel);
		
		pathLabel = new Label("File Path: ");
		pathLabel.setLocation(titleLabel.getX(), titleLabel.getY() + titleLabel.getHeight() + 20);
		pathLabel.setSize(80, 30);
		
		input = new TextField();
		input.setLocation(pathLabel.getX() + pathLabel.getWidth(), pathLabel.getY());
		input.setSize(600, 30);
		input.setBackground(Color.white);
		input.setForeground(Color.black);
		input.setFont(REGULAR_FONT);
		add(input);//do not put in regFormat: custom format
		
		select = new Button("...");
		select.setLocation(input.getX() + input.getWidth(), input.getY());
		select.setSize(30, 30);
		
		createNew = new Checkbox("Create New File?", defaultCreateNewFile);
		createNew.setLocation(input.getX(), input.getY() + input.getHeight() + 10);
		createNew.setSize(160, 30);
		if (!defaultIsChangeable) {
			createNew.setEnabled(false);
			createNew.setVisible(false);
		}
		
		submit = new Button("Submit");
		submit.setSize(100, 30);
		submit.setLocation(select.getX() + select.getWidth() - submit.getWidth(), createNew.getY() + createNew.getHeight() + 20);
		
		cancel = new Button("Cancel");
		cancel.setSize(100, 30);
		cancel.setLocation(submit.getX() - 10 - cancel.getWidth(), submit.getY());
		
		Component[] regFormat = {pathLabel, select, createNew, submit, cancel};
		for ( int i=0 ; i<regFormat.length ; i++ ) {
			regFormat[i].setFont(REGULAR_FONT);
			regFormat[i].setBackground(BACKGROUND_COLOR);
			regFormat[i].setForeground(FOREGROUND_COLOR);
			add(regFormat[i]);
		}
		
		click = (new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				mouseClick(e.getSource());
			}
		});
		
		select.addMouseListener(click);
		cancel.addMouseListener(click);
		submit.addMouseListener(click);
		
		setSize(submit.getWidth() + submit.getX() + 20, submit.getY() + submit.getHeight() + 20);
		if (parent != null) {
			setLocation(parent.getX(), parent.getY());
		} else {
			setLocation(10, 10);
		}
		
		setVisible(true);
	}
	
	private void mouseClick(Object target) {
		if (target == submit) {
			value = new File(input.getText());
			if (extension != null && !value.getAbsolutePath().substring(value.getAbsolutePath().length() - extension.length(), value.getAbsolutePath().length()).equalsIgnoreCase(extension)) {
				JOptionPane.showMessageDialog(this, "File must be of extension "+extension, "Invalid Input", JOptionPane.ERROR_MESSAGE);
				value = null;
				return;
			}
			if (value.exists() && createNew.getState()) {
				//file already exists but you are trying to create new one
				JOptionPane.showMessageDialog(this, "You cannot create this file because it already exists!", "Invalid Input", JOptionPane.ERROR_MESSAGE);
				value = null;
				return;
			} else if (!value.getParentFile().exists() && createNew.getState()) {
				//parent folder does not exist and create file
				JOptionPane.showMessageDialog(this, "This file cannot be created because it's folder does not exist!", "Invalid Input", JOptionPane.ERROR_MESSAGE);
				value = null;
				return;
			} else if (!value.exists() &&  !createNew.getState()) {
				//file does not exist and you are trying to open it
				JOptionPane.showMessageDialog(this, "You cannot open this file because it does not exist!", "Invalid Input", JOptionPane.ERROR_MESSAGE);
				value = null;
				return;
			}
			if (createNew.getState()) {
				try {
					value.createNewFile();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(this, "An unexplained, internal error ocurred while creating the file.", "Internal Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
					return;
				}
			}
			dispose();
		} else if (target == cancel) {
			value = null;
			dispose();
		} else if (target == select) {
			JFileChooser fs = new JFileChooser();
			fs.setSize(super.getWidth(), super.getHeight());
			fs.setLocation(super.getX(), super.getY());
			int res = JFileChooser.CANCEL_OPTION;
			if (createNew.getState()) {
				res = fs.showSaveDialog(this);
			} else {
				res = fs.showOpenDialog(this);
			}
			if (res == JFileChooser.APPROVE_OPTION) {
				value = fs.getSelectedFile();
				fs = null;
				if (extension != null && !value.getAbsolutePath().substring(value.getAbsolutePath().length() - extension.length(), value.getAbsolutePath().length()).equalsIgnoreCase(extension)) {
					JOptionPane.showMessageDialog(this, "File must be of extension "+extension, "Invalid Input", JOptionPane.ERROR_MESSAGE);
					value = null;
					return;
				}
				if (value.exists() && createNew.getState()) {
					//file already exists but you are trying to create new one
					JOptionPane.showMessageDialog(this, "You cannot create this file because it already exists!", "Invalid Input", JOptionPane.ERROR_MESSAGE);
					value = null;
					return;
				} else if (!value.getParentFile().exists() && createNew.getState()) {
					//parent folder does not exist and create file
					JOptionPane.showMessageDialog(this, "This file cannot be created because it's folder does not exist!", "Invalid Input", JOptionPane.ERROR_MESSAGE);
					value = null;
					return;
				} else if (!value.exists() &&  !createNew.getState()) {
					//file does not exist and you are trying to open it
					JOptionPane.showMessageDialog(this, "You cannot open this file because it does not exist!", "Invalid Input", JOptionPane.ERROR_MESSAGE);
					value = null;
					return;
				}
				input.setText(value.getAbsolutePath());
			}
		}
	}
	
	private File getValue() {
		return value;
	}
	
	public static File getFileOfExtension(Frame parent, String title, String extension) {
		return new FileSelector(parent, title, extension, false, true).getValue();
	}
	
	public static File getFile(Frame parent, String title) {
		return new FileSelector(parent, title, null, false, true).getValue();
	}
	
	public static File openExistingFile(Frame parent, String title) {
		return new FileSelector(parent, title, null, false, false).getValue();
	}
	
	public static File openNewFile(Frame parent, String title) {
		return new FileSelector(parent, title, null, true, false).getValue();
	}
	
	public static File openExistingFileOfExtension(Frame parent, String title, String extension) {
		return new FileSelector(parent, title, extension, false, false).getValue();
	}
	
	public static File openNewFileOfExtension(Frame parent, String title, String extension) {
		return new FileSelector(parent, title, extension, true, false).getValue();
	}
}
