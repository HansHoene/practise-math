// Program: PracticeMathClient.java
// Written by: Hans-Edward Hoene
// Description: 
// Challenges: 
// Time Spent: 
// Given Input:               Expected Output:
// --------------------          -------------------------
// 1.)
// 2.)
// 3.)
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// Mar 6, 2016        	 HEH       				Created

package hansedwardhoene.app;

import hansedwardhoene.app.practicemath.*;
import hansedwardhoene.database.DatabaseFormatException;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.Border;

/*filename: PracticeMathClient.java*/
public class PracticeMathClient extends java.awt.Frame implements MouseListener, KeyListener, WindowListener {
	
	private static final long serialVersionUID = 1L;

	private final static String dataFile = System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Practice Math";
	
	private final static Font titleFont = new Font("Times New Roman", 0 , 150);
	private final static Font questionFont = new Font("Bold", 0 , 120);
	private final static Font buttonFont = new Font("Bold", 0 , 48);
	private final static Font inputFont = new Font("Arial", 0 , 48);
	
	private Label title, question;
	private JTextField input;
	private Button submit, settings, history, change;
	
	private Color fontColor, backgroundColor, buttonFontColor, buttonBackgroundColor;
	private Border border;
	
	private Session session;
	
	private DataIO io;
	
	public PracticeMathClient(String username, String password) throws FileNotFoundException, IOException, DatabaseFormatException {
		super("Practice Math");
		
		/*{
			String tmp = null;
			try {
				tmp = System.getenv("PRACTICE_MATH_ADMIN_LOCK");
			} catch (Throwable e) {}
			if (tmp == null) {
				adminLock = true;
				try {
					Runtime.getRuntime().exec("SETX PRACTICE_MATH_ADMIN_LOCK ENABLED");
				} catch (Throwable e) {}
			} else {
				if (tmp.equalsIgnoreCase("DISABLED")) {
					adminLock = false;
				} else {
					adminLock = true;
				}
			}
		}*/
		
		session = new Session(Session.MIXED);//practice math session
		
		//set up fonts and colors
		fontColor = Color.white;
		backgroundColor = Color.black;
		buttonFontColor = Color.black;
		buttonBackgroundColor = Color.lightGray;
		border = BorderFactory.createLineBorder(Color.black, 1);
		
		io = new DataIO(username, password);
		
		SettingsConfiguration t = io.getSettingsConfiguration();
		fontColor = getFontColor(t.getBackgroundColor());
		backgroundColor = t.getBackgroundColor();
		buttonFontColor = getFontColor(t.getButtonBackgroundColor());
		buttonBackgroundColor = t.getButtonBackgroundColor();
		session.setMinimum(t.getMin());
		session.setMaximum(t.getMax());
		session.setType(t.getType());
		
		//set up super
		super.setLayout(null);
		super.setLocation(50, 50);
		
		//initialize
		title = new Label("Practice Math");
		question = new Label();
		input = new JTextField();
		submit = new Button("Check Answer");
		settings = new Button("Settings");
		history = new Button("History");
		change = new Button("Change Password");
		
		//set element sizes
		title.setSize(1000, 130);
		question.setSize(1000, 100);
		input.setSize(300, 60);
		submit.setSize(320, 60);
		settings.setSize(300, 60);
		history.setSize(300, 60);
		change.setSize(400, 60);
		
		title.setLocation(20, 60);
		question.setLocation(title.getX(), title.getY() + title.getHeight() + 30);
		input.setLocation(title.getX(), question.getY() + question.getHeight() + 10);
		submit.setLocation(input.getX() + input.getWidth() + 1, input.getY());
		settings.setLocation(title.getX() + 25, input.getY() + input.getHeight() + 10);
		history.setLocation(settings.getX(), settings.getY() + settings.getHeight() + 10);
		change.setLocation(history.getX(), history.getY() + history.getHeight() + 10);
		
		title.setFont(titleFont);
		question.setFont(questionFont);
		input.setFont(inputFont);
		submit.setFont(buttonFont);
		settings.setFont(buttonFont);
		history.setFont(buttonFont);
		change.setFont(buttonFont);
		
		input.setBorder(border);
		//submit.setBorder(border);
		//settings.setBorder(border);
		//history.setBorder(border);
		//change.setBorder(border);
		
		updateColors();
		
		submit.addMouseListener(this);
		settings.addMouseListener(this);
		history.addMouseListener(this);
		input.addKeyListener(this);
		change.addMouseListener(this);
		
		super.add(title);
		super.add(question);
		super.add(input);
		super.add(submit);
		super.add(settings);
		super.add(history);
		super.add(change);
		
		//super.setSize(submit.getX() + submit.getWidth() + 10, history.getY() + history.getHeight() + 40);
		super.setSize(1100, change.getY() + change.getHeight() + 30);
		
		super.addWindowListener(this);
		
		super.setTitle("Practice Math - "+username);
		
		question.setText(session.newQuestion());
	}
	
	public void updateColors() {
		super.setBackground(backgroundColor);
		title.setBackground(backgroundColor);
		question.setBackground(backgroundColor);
		input.setBackground(Color.white);//opposite
		submit.setBackground(buttonBackgroundColor);
		settings.setBackground(buttonBackgroundColor);
		history.setBackground(buttonBackgroundColor);
		change.setBackground(buttonBackgroundColor);
		
		super.setForeground(fontColor);
		title.setForeground(fontColor);
		question.setForeground(fontColor);
		input.setForeground(Color.black);//opposite
		submit.setForeground(buttonFontColor);
		settings.setForeground(buttonFontColor);
		history.setForeground(buttonFontColor);
		change.setForeground(buttonFontColor);
		
		super.revalidate();
		super.repaint();
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		Object target = arg0.getSource();
		if (target == submit) {
			checkAnswer();
		} else if (target == settings) {
			SettingsConfiguration c = new SettingsConfiguration();
			c.setBackgroundColor(backgroundColor);
			c.setButtonBackgroundColor(buttonBackgroundColor);
			c.setMin(session.getMinimum());
			c.setMax(session.getMaximum());
			c.setType(session.getType());
			c = SettingsForm.getConfiguration(this, "Change Configurations", c);
			if (c == null) return;
			backgroundColor = c.getBackgroundColor();
			buttonBackgroundColor = c.getButtonBackgroundColor();
			buttonFontColor = getFontColor(c.getButtonBackgroundColor());
			fontColor = getFontColor(c.getBackgroundColor());
			session.setMinimum(c.getMin());
			session.setMaximum(c.getMax());
			session.setType(c.getType());
			updateColors();
			question.setText(session.newQuestion());
			try {
				io.setSettingsConfiguration(c);
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			} catch (DatabaseFormatException e) {}
		} else if (target == history) {
			showHistory();
			question.setText(session.newQuestion());
		} else if (target == change) {
			Query n = LoginForm.changePassword(this, "Change Password");
			if (n == null) return;
			try {
				io.changePassword(n.getKey(), n.getValue());
			} catch (IllegalArgumentException err) {
				JOptionPane.showMessageDialog(this, err.getMessage());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, "An input/output error ocurred!");
			} catch (DatabaseFormatException e) {
				JOptionPane.showMessageDialog(this, "An internal error ocurred!");
			}
		}
	}
	
	@Override
	public void keyTyped(KeyEvent event) {
		char c = event.getKeyChar();
		event.consume();//cancel event
		if (event.getSource() != input) return;
		
		if (c == '\r' || c == '\n') { //enter
			checkAnswer();
		} else if (c == '\b') { //backspace
			//clear textfield
			input.setText("");
		} else if (c == '-') {
			//switch between negative and positive
			if (input.getText().contains("-")) {
				input.setText(input.getText().replaceAll("-", ""));
			} else {
				input.setText("-"+input.getText());
			}
		} else {
			//only allow number to be typed
			try {
				int num = Integer.parseInt(c+"");
				input.setText(input.getText()+""+num);
			} catch (NumberFormatException e) {}
		}
	}
	
	@Override
	public void windowClosing(WindowEvent arg0) {
		dispose();
	}
	
	@Override
	public void windowClosed(WindowEvent arg0) {
		System.exit(0);
	}
	
	public void checkAnswer() {
		int ans;
		try {
			ans = Integer.parseInt(input.getText());
		} catch (NumberFormatException e) {
			input.setText("");
			return;
		}
		
		try {
			io.addQuestion(session.getQuestion(), session.getAnswer(), ans);
		} catch (IOException | DatabaseFormatException e1) {
			JOptionPane.showMessageDialog(this, "An Input/Output Error has ocurred.");
			this.dispose();
		}
		if (session.checkAnswer(ans)) {
			question.setText(session.newQuestion());
		} else {
			
		}
		input.setText("");
		input.requestFocus();
	}
	
	public void showHistory() {
		try {
			super.setEnabled(false);
			super.setVisible(false);
			History.showHistory(this, "History", io);
			super.setVisible(true);
			super.setEnabled(true);
			super.requestFocus();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this, "An internal error has ocurred!");
			System.exit(0);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, "An internal error has ocurred!");
			System.exit(0);
		} catch (DatabaseFormatException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void dispose() {
		try {
			this.io.logout();
		} catch (Throwable e) {}
		super.dispose();
	}
	
	public static Color getFontColor(Color background) {
		double yiq = 0.299 * background.getRed() + 0.587 * background.getGreen() + 0.114 * background.getBlue();
		return yiq >= 128 ? Color.black : Color.white;
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException, DatabaseFormatException {
		new File(dataFile+"/AppData").mkdirs();
		Query up;
		PracticeMathClient client = null;
		int r = JOptionPane.showConfirmDialog(null, "Do you have an existing account?", "Account?", JOptionPane.YES_NO_CANCEL_OPTION);
		if (r == JOptionPane.YES_OPTION) {
			while (true) {
				up = LoginForm.getUsernamePasswordCombo(null, "Practice Math Login");
				if (up == null) System.exit(0);
				try {
					client = new PracticeMathClient(up.getKey(), up.getValue());
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					continue;
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (DatabaseFormatException e1) {
					e1.printStackTrace();
				}
				break;
			}
		} else if (r == JOptionPane.NO_OPTION) {
			
			while (true) {
				up = LoginForm.createUsernamePasswwordCombo(null, "Create Practice Math Account");
				if (up == null) System.exit(0);
				try {
					DataIO.newAccount(up.getKey(), up.getValue());
				} catch (Throwable e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					continue;
				}
				client = new PracticeMathClient(up.getKey(), up.getValue());
				break;
			}
		} else {
			System.exit(0);
		}
		client.setVisible(true);
	}
	
	//unneeded methods
	@Override public void windowDeactivated(WindowEvent arg0) {}
	@Override public void windowDeiconified(WindowEvent arg0) {}
	@Override public void windowIconified(WindowEvent arg0) {}
	@Override public void windowOpened(WindowEvent arg0) {}
	@Override public void windowActivated(WindowEvent arg0) {}

	@Override public void keyReleased(KeyEvent arg0) {}
	@Override public void keyPressed(KeyEvent event) {}
	
	@Override public void mouseEntered(MouseEvent arg0) {}
	@Override public void mouseExited(MouseEvent arg0) {}
	@Override public void mousePressed(MouseEvent arg0) {}
	@Override public void mouseReleased(MouseEvent arg0) {}
	
}