package hansedwardhoene.app.practicemath;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;

import javax.swing.JProgressBar;

public class ProgressBar {
	
	private final static Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	
	private JProgressBar p;
	private Dialog d;
	private String title;
	
	public ProgressBar(String title, Frame parent) {
		p = new JProgressBar(0, 100);
		d = new Dialog(parent, title, false);
		
		this.title = title;
		
		d.setLayout(null);
		
		p.setSize(500, 50);
		p.setLocation(10, 30);
		p.setValue(0);
		p.setString(null);
		p.setStringPainted(true);
		p.setBackground(Color.white);
		p.setForeground(Color.green);
		
		d.setSize(520, 90);
		d.setLocation((int) ((SCREEN_SIZE.getWidth() - d.getWidth()) / 2), (int) ((SCREEN_SIZE.getHeight() - d.getHeight()) / 2));
		
		d.add(p);
		d.setVisible(true);
	}
	
	public ProgressBar(String title, Dialog parent) {
		p = new JProgressBar(0, 100);
		d = new Dialog(parent, title, false);
		
		this.title = title;
		
		d.setLayout(null);
		
		p.setSize(500, 50);
		p.setLocation(10, 30);
		p.setValue(0);
		p.setString(null);
		p.setStringPainted(true);
		p.setBackground(Color.white);
		p.setForeground(Color.green);
		
		d.setSize(520, 90);
		d.setLocation((int) ((SCREEN_SIZE.getWidth() - d.getWidth()) / 2), (int) ((SCREEN_SIZE.getHeight() - d.getHeight()) / 2));
		
		d.add(p);
		d.setVisible(true);
	}
	
	public void show() {
		p.setVisible(true);
		d.setVisible(true);
	}
	
	public void hide() {
		p.setVisible(false);
		d.setVisible(false);
	}
	
	public void setTitle(String t) {
		this.title = t;
	}
	
	public void setPercentValue(int v) {
		p.setValue(v);
		d.setTitle(title+" "+v+"%");
	}
	
	public int getPercentValue() {
		return p.getValue();
	}
	
	public void dispose() {
		d.dispose();
	}
}