package hansedwardhoene.app.practicemath;

import hansedwardhoene.app.PracticeMathClient;

import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JColorChooser;
import javax.swing.JOptionPane;

public class SettingsForm extends java.awt.Dialog {
	//written by Hans-Edward Hoene
	
	private static final long serialVersionUID = 0L;

	private SettingsConfiguration value;
	
	private Label title, cL, bcL, minL, maxL, typeL;
	private Button c, bc;
	private NumberField min, max;
	private Choice type;
	private Button submit;
	
	private SettingsForm(Frame parent, String title, SettingsConfiguration config) {
		super(parent, title, true);
		super.setLayout(null);
		
		this.value = config;
		
		if (value == null) {
			value = new SettingsConfiguration();
			value.setBackgroundColor(Color.black);
			value.setButtonBackgroundColor(Color.gray);
			value.setMax(12);
			value.setMin(0);
			value.setType(Session.MIXED);
		}
		
		this.title = new Label("Settings");
		this.title.setSize(300, 30);
		this.title.setLocation(10, 50);
		this.title.setFont(new Font("Bold", 0, 24));
		this.title.setForeground(Color.black);
		this.title.setBackground(Color.white);
		super.add(this.title);
		
		this.cL = new Label("Color: ");
		this.bcL = new Label("Button Color:");
		this.minL = new Label("Minimum Number:");
		this.maxL = new Label("Maximum Number:");
		this.typeL = new Label("Question Type:");
		
		this.c = new Button("Font");this.c.setBackground(value.getBackgroundColor());this.c.setForeground(PracticeMathClient.getFontColor(value.getBackgroundColor()));
		this.bc = new Button("Font");this.bc.setBackground(value.getButtonBackgroundColor());this.bc.setForeground(PracticeMathClient.getFontColor(value.getButtonBackgroundColor()));
		this.min = new NumberField(value.getMin()+"");
		this.max = new NumberField(value.getMax()+"");
		this.type = new Choice();
		
		{
			String[] opts = {"Mixed","Addition","Subtraction","Multiplication","Division","Addition and Subtraction","Multiplication and Division"};
			for ( int i=0 ; i<opts.length ; i++ ) {
				this.type.addItem(opts[i]);
			}
			this.type.select(value.getType());
		}
		
		Label[] labels = {cL, bcL, minL, maxL, typeL};
		Component[] inputs = {c, bc, min, max, type};
		
		MouseListener colorClick = (new MouseListener() {

			@Override
			public void mouseClicked(java.awt.event.MouseEvent e) {
				mouseClick((Button)e.getSource());
			}

			@Override public void mouseEntered(java.awt.event.MouseEvent arg0) {}
			@Override public void mouseExited(java.awt.event.MouseEvent arg0) {}
			@Override public void mousePressed(java.awt.event.MouseEvent arg0) {}
			@Override public void mouseReleased(java.awt.event.MouseEvent arg0) {}
		});
		
		for ( int i=0 ; i<labels.length ; i++ ) {
			labels[i].setSize(150, 30);
			if (inputs[i] instanceof TextField) inputs[i].setSize(100, 30);
			else inputs[i].setSize(200, 30);
			if (inputs[i] instanceof Button) {
				inputs[i].addMouseListener(colorClick);
			}
			labels[i].setLocation(10, this.title.getY() + this.title.getHeight() + 30 + (i * (labels[i].getHeight() + 20)));
			inputs[i].setLocation(labels[i].getX() + labels[i].getWidth() + 10, labels[i].getY());
			labels[i].setForeground(Color.black);
			labels[i].setBackground(Color.white);
			super.add(labels[i]);
			super.add(inputs[i]);
		}
		
		submit = new Button("Submit");
		submit.setSize(200, 30);
		submit.setLocation(20, labels[labels.length - 1].getY() + labels[labels.length - 1].getHeight() + 30);
		submit.addMouseListener(new MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				value.setBackgroundColor(c.getBackground());
				value.setButtonBackgroundColor(bc.getBackground());
				try {
					value.setMax(Integer.parseInt(max.getText()));
					value.setMin(Integer.parseInt(min.getText()));
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Invalid Number Entered!");
					return;
				}
				value.setType(type.getSelectedIndex());
				dispose();
			}
		});
		super.add(submit);
		
		super.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				value = null;
				dispose();
			}
		});
		
		super.setSize(600, submit.getY() + submit.getHeight() + 20);
		if (parent == null) {
			super.setLocation(10, 10);
		} else {
			super.setLocation(parent.getLocation());
		}
		super.setForeground(Color.black);
		super.setBackground(Color.white);
		super.setVisible(true);
		
	}
	
	private void mouseClick(Button target) {
		Color b = JColorChooser.showDialog(this, "Choose Background Color", target.getBackground());
		if (b == null) return;
		//measure of brightness
		double yiq = 0.299 * b.getRed() + 0.587 * b.getGreen() + 0.114 * b.getBlue();
		//Color f = options.getFontColor(this, "Choose Font Color", b);
		Color f = yiq >= 128 ? Color.black : Color.white;
		target.setBackground(b);
		target.setForeground(f);
	}
	
	private SettingsConfiguration getValue() {
		return value;
	}
	
	/*private static class options extends java.awt.Dialog {
		
		
		private static final long serialVersionUID = 1L;
		
		private Label t;
		private Button sug, opp;
		private Color value;
		
		private static Font font = new Font("Arial", Font.BOLD, 48);
		
		private options(java.awt.Dialog parent, String title, Color background) {
			super(parent, title, true);
			super.setLayout(null);
			
			//measure of brightness
			double yiq = 0.299 * background.getRed() + 0.587 * background.getGreen() + 0.114 * background.getBlue();
			
			Color opposite = new Color(255 - background.getRed(), 255 - background.getGreen(), 255 - background.getBlue());
			Color suggested = yiq >= 128 ? Color.black : Color.white;
			
			t = new Label("Select a Font Display");
			sug = new Button("Suggested Font");
			opp = new Button("Opposite Font");
			
			super.setForeground(Color.black);
			super.setBackground(Color.white);
			t.setForeground(Color.black);
			t.setBackground(Color.white);
			
			sug.setForeground(suggested);
			opp.setForeground(opposite);
			
			sug.setBackground(background);
			opp.setBackground(background);
			
			t.setFont(font);
			sug.setFont(font);
			opp.setFont(font);
			
			t.setSize(500, 60);
			sug.setSize(400, 60);
			opp.setSize(400, 60);
			
			t.setLocation(20, 60);
			sug.setLocation(t.getX() + 10, t.getY() + t.getHeight() + 30);
			opp.setLocation(sug.getX(), sug.getY() + sug.getHeight() + 20);
			
			super.addWindowListener(new WindowListener() {
				@Override
				public void windowClosing(WindowEvent arg0) {
					value = null;
					dispose();
				}
				@Override public void windowActivated(WindowEvent arg0) {}
				@Override public void windowClosed(WindowEvent arg0) {}
				@Override public void windowDeactivated(WindowEvent arg0) {}
				@Override public void windowDeiconified(WindowEvent arg0) {}
				@Override public void windowIconified(WindowEvent arg0) {}
				@Override public void windowOpened(WindowEvent arg0) {}
			});
			
			MouseListener click = (new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent arg0) {
					Object target = arg0.getSource();
					if (target == sug) {
						value = suggested;
						dispose();
					} else if (target == opp) {
						value = opposite;
						dispose();
					}
				}
				
				@Override public void mouseEntered(MouseEvent arg0) {}
				@Override public void mouseExited(MouseEvent arg0) {}
				@Override public void mousePressed(MouseEvent arg0) {}
				@Override public void mouseReleased(MouseEvent arg0) {}
			});
			
			sug.addMouseListener(click);
			
			if (!background.equals(opposite)) {
				opp.addMouseListener(click);
			} else if (opposite.equals(suggested)) {
				value = suggested;
				dispose();
			} else {
				value = suggested;
				dispose();
			}
			
			super.add(t);
			super.add(sug);
			super.add(opp);
			
			super.setLocation(parent == null ? 200 : parent.getX(), parent == null ? 200 : parent.getY());
			super.setSize(550, opp.getY() + opp.getHeight() + 40);
			
			super.setVisible(true);
		}
		
		private Color getValue() {
			return value;
		}
		
		public static Color getFontColor(java.awt.Dialog d, String title, Color background) {
			if (background.equals(Color.white)) {
				return Color.black;
			} else if (background.equals(Color.black)) {
				return Color.white;
			} else {
				return new options(d, title, background).getValue();
			}
		}
	}*/
	
	public static SettingsConfiguration getConfiguration(Frame parent, String title, SettingsConfiguration config) {
		return new SettingsForm(parent, title, config).getValue();
	}
}
