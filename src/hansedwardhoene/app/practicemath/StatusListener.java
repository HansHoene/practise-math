package hansedwardhoene.app.practicemath;

public interface StatusListener {
	
	public void updateProgress(double p);
	
}