package hansedwardhoene.app.practicemath;

public class Query {
	
	private String key, value;
	
	public Query(String key, String value) {
		this.setKey(key);
		this.setValue(value);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return key+"="+value;
	}
	
	/*private ArrayList<String> key;
	private ArrayList<String> value;
	
	public Query() {
		key = new ArrayList<String>();
		value = new ArrayList<String>();
	}
	
	public Query(String queryString) {
		this();
		parse(queryString);
	}
	
	public void add(String key, String value) {
		this.key.add(key);
		this.value.add(value);
	}
	
	public void remove(String key) {
		int i = this.key.indexOf(key);
		if (i < 0 || i >= this.key.size()) throw new IndexOutOfBoundsException();
		this.key.remove(i);
		this.value.remove(i);
	}
	
	public void remove(int index) {
		if (index < 0 || index >= this.key.size()) throw new IndexOutOfBoundsException();
		this.key.remove(index);
		this.value.remove(index);
	}
	
	public void parse(String q) {
		String[] temp, s = q.split("&");
		for ( int i=0 ; i<s.length ; i++ ) {
			temp = s[i].split("=");
			if (temp.length == 1) {
				add(temp[0], "");
			} else if (temp.length > 2) {
				String t = "";
				for ( int j=1 ; j<temp.length ; j++ ) {
					t += temp[j];
				}
				add(temp[0], t);
			} else if (temp.length == 2) {
				add(temp[0],temp[1]);
			}
		}
	}
	
	public String get(String key) {
		try {
			return this.value.get(this.key.indexOf(key));
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	@Override
	public String toString() {
		String r = "";
		for ( int i=0 ; i<key.size() ; i++ ) {
			r += key.get(i)+"="+value.get(i);
			if (i != (key.size() - 1)) r += "&";
		}
		return r;
	}*/
}