package hansedwardhoene.app.practicemath;

import java.awt.event.MouseEvent;

public interface TableListener {
	
	public void cellClicked(MouseEvent e, int row, int col);
	
	public void cellValueChanged(String newVal, int row, int col);
}