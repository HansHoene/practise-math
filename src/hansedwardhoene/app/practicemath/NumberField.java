package hansedwardhoene.app.practicemath;

import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class NumberField extends TextField implements KeyListener {

	private static final long serialVersionUID = 1L;
	
	public NumberField(String txt) {
		super();
		super.addKeyListener(this);
		super.setText(txt);
	}
	
	public NumberField() {
		this("");
	}
	
	@Override
	public void keyTyped(KeyEvent event) {
		char c = event.getKeyChar();
		event.consume();//cancel event
		if (event.getSource() != this) return;
		
		if (c == '\r' || c == '\n') { //enter
			
		} else if (c == '\b') { //backspace
			//clear textfield
			super.setText("");
		} else if (c == '-') {
			//switch between negative and positive
			if (super.getText().contains("-")) {
				super.setText(super.getText().replaceAll("-", ""));
			} else {
				super.setText("-"+super.getText());
			}
		} else {
			//only allow number to be typed
			try {
				int num = Integer.parseInt(c+"");
				super.setText(super.getText()+""+num);
			} catch (NumberFormatException e) {}
		}
	}

	@Override public void keyPressed(KeyEvent arg0) {}
	@Override public void keyReleased(KeyEvent arg0) {}
}