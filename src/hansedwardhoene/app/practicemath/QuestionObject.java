package hansedwardhoene.app.practicemath;

public class QuestionObject {
	//written by Hans-Edward Hoene
	
	private String question;
	private int answer;
	private int input;
	
	public QuestionObject(String question, int answer, int input) {
		this.setQuestion(question);
		this.setAnswer(answer);
		this.setInput(input);
	}
	
	public QuestionObject() {
		question = "";
		answer = -1;
		input = -1;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		if (!question.matches("(\\-|)[0-9]{1,}( |)(\\+|\\-|\\�|\\�)( |)(\\-|)[0-9]{1,}( |)=( |)")) throw new IllegalArgumentException("Invalid Question!");
		this.question = question;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}

	public int getInput() {
		return input;
	}

	public void setInput(int input) {
		this.input = input;
	}
	
	public String stringify() {
		return "{p:"+this.question+",a:"+this.answer+",i:"+this.input+"}";
	}
	
	public void parse(String data) {
		if (!data.matches("\\{p:(\\-|)[0-9]{1,}( |)(\\+|\\-|\\�|\\�)( |)(\\-|)[0-9]{1,}( |)=( |),a:(\\-|)[0-9]{1,},i:(\\-|)[0-9]{1,}\\}")) throw new IllegalArgumentException("Incorrect Question Object Syntax");
		String[] s = data.replaceAll("\\{", "").replaceAll("\\}", "").split(",");
		this.setQuestion(s[0].replaceAll("p:", ""));
		this.setAnswer(Integer.parseInt(s[1].replaceAll("a:", "")));
		this.setInput(Integer.parseInt(s[2].replaceAll("i:", "")));
	}
	
	@Override
	public String toString() {
		return this.question+""+this.answer+"; "+this.input;
	}
}