package hansedwardhoene.app.practicemath;

import hansedwardhoene.database.DatabaseFormatException;
import hansedwardhoene.database.StatusListener;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;

public class History extends java.awt.Dialog implements TableListener, StatusListener {
	//written by Hans-Edward Hoene
	
	private static final long serialVersionUID = 1L;
	
	private Table table;
	
	private ProgressBar progress;
	private long size;
	
	private DataIO io;
	
	private History(Frame parent, String title, DataIO io, int day, int month, int year) throws FileNotFoundException, IOException, DatabaseFormatException {
		super(parent, title, true);
		
		this.table = day < 1 || month <1 || year < 1 ? io.loadData(this) : io.loadDataFromDate(day, month, year, this);
		if (day < 1 || month < 1 || year < 1)
			table.addTableListener(this);
		/*System.out.println(table.getLastRow()+", "+table.getLastColumn()+"\n");
		for ( int i=1 ; i<= table.getLastRow() ; i++ ) {
			for ( int j=1 ; j<table.getLastColumn() ; j++ ) {
				System.out.print(table.getValue(i, j)+", ");
			}
			System.out.println();
		}*/
		
		this.io = io;
		
		super.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
		
		//set table dimensions
		table.setRowSize(50);
		table.setColSize(200);
		table.setSize(850, 800);
		table.setLocation(0, 0);
		table.setVisible(true);
		super.add(table);
		
		super.setSize(850, 800);
		super.setVisible(true);

	}
	
	private History(Dialog parent, String title, DataIO io, int day, int month, int year) throws FileNotFoundException, IOException, DatabaseFormatException {
		super(parent, title, true);
		
		this.table = day < 1 || month <1 || year < 1 ? io.loadData(this) : io.loadDataFromDate(day, month, year, this);
		
		this.io = io;
		
		super.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
		
		//set table dimensions
		table.setRowSize(50);
		table.setColSize(200);
		table.setSize(850, 800);
		table.setLocation(0, 0);
		super.add(table);
		
		super.setSize(850, 800);
		super.setVisible(true);

	}

	
	
	public static void showHistory(Frame parent, String title, DataIO io) throws FileNotFoundException, IOException, DatabaseFormatException {
		new History(parent, title, io, -1, -1, -1);
	}
	
	public static void showHistoryForDate(Dialog parent, String title, DataIO io, int day, int month, int year) throws IOException, DatabaseFormatException {
		new History(parent, title, io, day, month, year);
	}

	@Override
	public void cellClicked(MouseEvent e, int row, int col) {
		if (row == 0) return;
		try {
			String[] s = table.getValue(row, 1).split("/");
			int month, day, year;
			month = Integer.parseInt(s[0]);
			day = Integer.parseInt(s[1]);
			year = Integer.parseInt(s[2]);
			super.setEnabled(false);
			History.showHistoryForDate(this, "History for "+month+"/"+day+"/"+year, this.io, day, month, year);
			super.setEnabled(true);
			super.requestFocus();
		} catch (IOException e1) {e1.printStackTrace();} catch (DatabaseFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	@Override public void cellValueChanged(String newVal, int row, int col) {}

	@Override
	public void updateProgress(double p) {
		if (this.progress != null) (new Thread() {
			public void run() {
				progress.setPercentValue((int) (50 * p / size));
			}
		}).start();
	}
}