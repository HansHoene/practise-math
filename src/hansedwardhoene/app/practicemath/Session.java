package hansedwardhoene.app.practicemath;

public class Session {
	//written by Hans-Edward Hoene
	
	public final static int ADDITION = 1;
	public final static int SUBTRACTION = 2;
	public final static int MULTIPLICATION = 3;
	public final static int DIVISION = 4;
	
	public final static int ADD_SUB = 5;
	public final static int MUL_DIV = 6;
	public final static int MIXED = 0;
	
	private int type;
	private int right, wrong;
	
	private int min, max;
	
	private String question;
	private int answer;
	
	private java.util.Random random;
	
	public Session() {
		this(MIXED);
	}
	
	public Session(int type) {
		this.type = Math.abs(type) % 7;
		this.right = this.wrong = 0;
		this.random = new java.util.Random();
		min = 0;max = 12;
		question = null;
		newQuestion();
	}
	
	public void setType(int type) {
		this.type = Math.abs(type) % 7;
	}
	
	public int getType() {
		return type;
	}
	
	public void setMaximum(int max) {
		this.max = max;
	}
	
	public void setMinimum(int min) {
		this.min = min;
	}
	
	public int getMaximum() {
		return max;
	}
	
	public int getMinimum() {
		return min;
	}
	
	public int getRightAnswers() {
		return right;
	}
	
	public int getWrongAnswers() {
		return wrong;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public int getScore() {
		return (int)Math.round((double)right * 100 / (double)(wrong + right));
	}
	
	public String newQuestion() {
		//generates question
		
		/*determine question type, generate random numbers, determine answer and return question*/
		int t, num1, num2;
		
		//determine question type
		switch (type) {
			case ADD_SUB:
				t = random.nextBoolean() ? ADDITION : SUBTRACTION;
				break;
			case MUL_DIV:
				t = random.nextBoolean() ? MULTIPLICATION : DIVISION;
				break;
			case MIXED:
				t = random.nextBoolean() ? (random.nextBoolean() ? ADDITION : SUBTRACTION) : (random.nextBoolean() ? MULTIPLICATION : DIVISION);
				break;
			default:
				t = type;
		}
		
		//generate random numbers from given range
		num1 = random.nextInt(max - min + 1) + min;
		num2 = random.nextInt(max - min + 1) + min;
		if (t == DIVISION) {
			while (num1 == 0) {
				num1 = random.nextInt(max - min + 1) + min;
			}
		}
		
		//determine answer and return question
		switch (t) {
			case ADDITION:
				answer = num1 + num2;
				return question = num1+" + "+num2+" = ";
			case SUBTRACTION:
				answer = num2;
				return question = (num1 + num2)+" - "+num1+" = ";
			case MULTIPLICATION:
				answer = num1 * num2;
				return question = num1+" � "+num2+" = ";
			case DIVISION:
				answer = num2;
				return question = (num1 * num2)+" � "+num1+" = ";
		}
		
		return null;
	}
	
	public boolean checkAnswer(int ans) {
		//checks answer to previous question and returns new question
		if (question == null) return false;
		
		if (ans == answer) {
			right++;
			return true;
		} else {
			wrong++;
			return false;
		}
	}

	public int getAnswer() {
		return answer;
	}
}