package hansedwardhoene.app.practicemath;

import java.awt.Color;

public class SettingsConfiguration {
	//written by Hans-Edward Hoene
	
	private Color backgroundColor, buttonBackgroundColor;
	
	private int min, max, type;
	
	public SettingsConfiguration() {
		this.backgroundColor = this.buttonBackgroundColor = null;
		this.min = this.max = this.type = 0;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Color getButtonBackgroundColor() {
		return buttonBackgroundColor;
	}

	public void setButtonBackgroundColor(Color buttonBackgroundColor) {
		this.buttonBackgroundColor = buttonBackgroundColor;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}