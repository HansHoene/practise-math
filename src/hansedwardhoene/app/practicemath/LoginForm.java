package hansedwardhoene.app.practicemath;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;

import javax.swing.JOptionPane;

public class LoginForm extends java.awt.Dialog {
	//written by Hans-Edward Hoene
	
	private static final long serialVersionUID = 1L;
	
	private final static int LOGIN = 0, CREATE_ACCOUNT = 1, CHANGE_PASSWORD = 2;
	
	private Label usernameL, passwordL, passwordVL;
	private TextField username, password, passwordV;
	private Button submit;
	
	private Query value;
	
	private int purpose;

	private LoginForm(Frame parent, String title, int purpose) {
		super(parent, title, true);
		super.setLayout(null);
		
		Font f = new Font("Bold", 0, 36);
		
		this.purpose = purpose % 3;
		
		usernameL = new Label("Username:");
		passwordL = new Label("Password:");
		passwordVL = new Label("Verify Password: ");
		username = new TextField(20);
		password = new TextField();password.setEchoChar('X');
		passwordV = new TextField();passwordV.setEchoChar('X');
		submit = new Button("OK");
		
		java.awt.Component[] c = {usernameL, passwordL, passwordVL, username, password, passwordV, submit};
		
		for ( int i=0 ; i<c.length ; i++ ) {
			c[i].setForeground(Color.black);
			c[i].setBackground(Color.white);
			c[i].setFont(f);
		}
		
		super.setForeground(Color.black);
		super.setBackground(Color.white);
		
		usernameL.setSize(270, 60);
		passwordL.setSize(270, 60);
		passwordVL.setSize(270, 60);
		username.setSize(250, 60);
		password.setSize(250, 60);
		passwordV.setSize(250,60);
		submit.setSize(150, 60);
		
		usernameL.setLocation(10, 60);
		passwordL.setLocation(10, 130);
		if (purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD) passwordVL.setLocation(10, 200);
		username.setLocation(290, 60);
		password.setLocation(290, 130);
		if (purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD) passwordV.setLocation(290, 200);
		submit.setLocation(20, purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD ? 270 : 200);
		
		super.add(usernameL);
		super.add(passwordL);
		if (purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD) super.add(passwordVL);
		super.add(username);
		super.add(password);
		if (purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD) super.add(passwordV);
		super.add(submit);
		
		super.setSize(600, purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD ? 390 : 320);
		super.setLocation(parent == null ? 200 : parent.getX(), parent == null ? 200 : parent.getY());
		
		super.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent e) {
				value = null;
				dispose();
			}
		});
		
		submit.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent e) {
				submit();
			}
		});
		
		if (purpose == CHANGE_PASSWORD) {
			usernameL.setText("Old Password:");
			username.setEchoChar('X');
		}
		
		super.setVisible(true);
	}
	
	private void submit() {
		if ((purpose == CREATE_ACCOUNT || purpose == CHANGE_PASSWORD) && !password.getText().equals(passwordV.getText())) {
			//passwords don't match
			JOptionPane.showMessageDialog(this, "Your passwords don't match!");
			return;
		} else if ((purpose == CREATE_ACCOUNT) && (!username.getText().matches("[A-Za-z0-9]{4,10}") || !password.getText().matches("[A-Za-z0-9]{4,10}"))) {
			//invalid username or password when creating account
			JOptionPane.showMessageDialog(this, "Your username and password may only contain letters and numbers, and they must be between 4 and 10 characters long.");
			return;
		} else if (purpose == CHANGE_PASSWORD && !password.getText().matches("[A-Za-z0-9]{4,10}")) {
			//invalid password when changing it
			JOptionPane.showMessageDialog(this, "Your username and password may only contain letters and numbers, and they must be between 4 and 10 characters long.");
			return;
		}
		value = new Query(username.getText(), password.getText());
		dispose();
	}
	
	private Query getValue() {
		return value;
	}
	
	public static Query getUsernamePasswordCombo(Frame parent, String title) {
		//returns username=password
		return new LoginForm(parent, title, LOGIN).getValue();
	}
	
	public static Query createUsernamePasswwordCombo(Frame parent, String title) {
		//returns username=password
		return new LoginForm(parent, title, CREATE_ACCOUNT).getValue();
	}
	
	public static Query changePassword(Frame parent, String title) {
		//returns oldpassword=newpassword
		return new LoginForm(parent, title, CHANGE_PASSWORD).getValue();
	}
	
	
}
