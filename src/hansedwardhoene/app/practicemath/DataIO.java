package hansedwardhoene.app.practicemath;

import hansedwardhoene.cryptography.CryptographyByteManager;
import hansedwardhoene.database.DatabaseFormatException;
import hansedwardhoene.database.RecordDatabase;
import hansedwardhoene.database.StatusListener;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

public class DataIO {
	
	//"Username", "Password", "Date of Account (day/month/year)", "User Manifest Path", "Background Color", "Button Background Color", "Min", "Max", "Type"
	//Day	Month	Year	Path	length
	//question	answer	input
	
	public final static String APP_DATA = System.getenv("USERPROFILE")+
		"/Hoene Multipurpose Software Development Company"+
			"/Practice Math/AppData";
	
	private final static Random RANDOM = new Random();
	
	private final static int DAY, MONTH, YEAR;
	
	private final static String EXTENSION = ".data";
	
	private final static File MANIFEST_FILE = new File(APP_DATA+"/manifest");
	
	private final static CryptographyByteManager WORKER = new CryptographyByteManager(16, 5, 53, 299, true);
	
	private File userFile;
	private RecordDatabase userData;
	private RecordDatabase currentData;
	
	private String user, pass;
	
	private SettingsConfiguration settings;
	
	static {
		new File(APP_DATA).mkdirs();
		if (!MANIFEST_FILE.exists() || !MANIFEST_FILE.isFile()) {
			try {
				MANIFEST_FILE.createNewFile();
				RecordDatabase.newEncryptedDatabase(MANIFEST_FILE, WORKER, 
						"Username", "Password", "Date of Account (day/month/year)", "User Manifest Path", "Background Color", "Button Background Color", "Min", "Max", "Type")
						.close();
			} catch (IOException e) {
				e.printStackTrace();
				javax.swing.JOptionPane.showMessageDialog(null, "An Input/Output Error Ocurred!", "Innternal Error", javax.swing.JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			} catch (DatabaseFormatException e) {
				e.printStackTrace();
				javax.swing.JOptionPane.showMessageDialog(null, "An Input/Output Error Ocurred!", "Innternal Error", javax.swing.JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		}
		GregorianCalendar c = new GregorianCalendar();
		DAY = c.get(Calendar.DAY_OF_MONTH);
		MONTH = c.get(Calendar.MONTH) + 1;
		YEAR = c.get(Calendar.YEAR);
	}
	
	public static void newAccount(String username, String password) throws FileNotFoundException, IOException, DatabaseFormatException {
		RecordDatabase data = RecordDatabase.loadEncryptedDatabase(MANIFEST_FILE, WORKER);
		
		String[] line;
		
		while (data.hasNext()) {
			line = data.next();
			if (line[0].equals(username)) {
				throw new IllegalArgumentException("Username already exists!");
			}
		}
		
		File x;
		do {
			x = new File(APP_DATA+"/"+generateCode()+""+EXTENSION);
		} while(x.exists());
		x.createNewFile();
		
		data.addRecord(username, password, DAY+"/"+MONTH+"/"+YEAR, x.getName(), stringifyColor(Color.black), stringifyColor(Color.lightGray), 0+"", 12+"", Session.MIXED+"");
		data.close();
		data = RecordDatabase.newEncryptedDatabase(x, WORKER, "Day", "Month", "Year", "Path", "Length");
		data.close();
		data = null;
	}
	
	public DataIO(String username, String password) throws FileNotFoundException, IOException, DatabaseFormatException {
		RecordDatabase data = RecordDatabase.loadEncryptedDatabase(MANIFEST_FILE, WORKER);
		
		String[] line = null;

		this.user = username;
		this.pass = password;
		
		settings = new SettingsConfiguration();
		settings.setBackgroundColor(Color.black);
		settings.setButtonBackgroundColor(Color.lightGray);
		settings.setMin(0);
		settings.setMax(12);
		settings.setType(Session.MIXED);
		
		int num = 0;
		Color c = null;
		while (data.hasNext()) {
			line = data.next();
			if (line[0].equals(username)) {
				pass = line[1];
				
				c = parseColor(line[4]);
				if (c != null) settings.setBackgroundColor(c);
				c = parseColor(line[5]);
				if (c != null) settings.setButtonBackgroundColor(c);
				try { num = Integer.parseInt(line[6]);settings.setMin(num); } catch (NumberFormatException err) {}
				try { num = Integer.parseInt(line[7]);settings.setMax(num); } catch (NumberFormatException err) {}
				try { num = Integer.parseInt(line[8]);settings.setType(num); } catch (NumberFormatException err) {}
				//"Username", "Password", "Date of Account (day/month/year)", "User Manifest Path", "Background Color", "Button Background Color", "Min", "Max", "Type"
				break;
			}
			line = null;
		}
		
		if (line == null)
			throw new IllegalArgumentException("Username does not exist!");
		else if (!password.equals(line[1]))
			throw new IllegalArgumentException("Incorrect password!");
		
		this.userFile = new File(APP_DATA+"/"+line[3]);
		if (!userFile.exists() || !userFile.isFile())
			throw new InternalError("An internal error ocurred.");
		
		data.close();
		
		this.userData = RecordDatabase.loadEncryptedDatabase(userFile, WORKER);
		
		String path = null;
		
		while (userData.hasNext()) {
			line = userData.next();
			if (line[0].equals(""+DAY) && line[1].equals(""+MONTH) && line[2].equals(""+YEAR)) {
				path = line[3];
			}
		}
		if (path == null) {
			File x;
			do {
				x = new File(APP_DATA+"/"+generateCode()+""+EXTENSION);
			} while (x.exists());
			x.createNewFile();
			
			currentData = RecordDatabase.newEncryptedDatabase(x, WORKER, 
					"Question", "Answer", "Input");
			
			userData.addRecord(DAY+"", MONTH+"", YEAR+"", x.getName(), currentData.length()+"");
		} else {
			File x = new File(APP_DATA+"/"+path);
			if (!x.exists() || !x.isFile())
				throw new InternalError();
			
			currentData = RecordDatabase.loadEncryptedDatabase(x,  WORKER);
		}
		userData.reset();
		
		while (userData.hasNext()) {
			line = userData.next();
			if (line[0].equalsIgnoreCase(""+DAY) && line[1].equalsIgnoreCase(""+MONTH) && line[2].equalsIgnoreCase(""+YEAR)) {
				userData.editRecord(line, 
						DAY+"", MONTH+"", YEAR+"", line[3], currentData.length()+"");
				break;
			}
		}
		
		userData.reset();
	}
	
	public SettingsConfiguration getSettingsConfiguration() {
		SettingsConfiguration r = new SettingsConfiguration();
		r.setBackgroundColor(settings.getBackgroundColor());
		r.setButtonBackgroundColor(settings.getButtonBackgroundColor());
		r.setMin(settings.getMin());
		r.setMax(settings.getMax());
		r.setType(settings.getType());
		return r;
	}
	
	public void setSettingsConfiguration(SettingsConfiguration r) throws FileNotFoundException, IOException, DatabaseFormatException {
		RecordDatabase d = RecordDatabase.loadEncryptedDatabase(MANIFEST_FILE, WORKER);
		
		String[] line = null;
		while (d.hasNext()) {
			line = d.next();
			if (line[0].equals(user) && line[1].equals(pass)) break;
			line = null;
		}
		if (line == null) {
			d.close();
			return;
		}
		
		d.editRecord(line, line[0], line[1], line[2], line[3], stringifyColor(r.getBackgroundColor()), stringifyColor(r.getButtonBackgroundColor()), r.getMin()+"", r.getMax()+"", r.getType()+"");
		settings.setBackgroundColor(r.getBackgroundColor());
		settings.setButtonBackgroundColor(r.getButtonBackgroundColor());
		settings.setMin(r.getMin());
		settings.setMax(r.getMax());
		settings.setType(r.getType());
		d.close();
		d = null;
	}
	
	public void changePassword(String oldpassword, String newpassword) throws IOException, DatabaseFormatException {
		if (!pass.equals(oldpassword)) throw new IllegalArgumentException("Incorrect password!");
		
		RecordDatabase d = RecordDatabase.loadEncryptedDatabase(MANIFEST_FILE,  WORKER);
		
		String[] line = null;
		while (d.hasNext()) {
			line = d.next();
			if (line[0].equals(user)) break;
			line = null;
		}
		d.editRecord(line, line[0], newpassword, line[2], line[3], line[4], line[5], line[6], line[7], line[8]);
		pass = newpassword;
		d.close();
		d = null;
	}
	
	public void addQuestion(String question, int answer, int input) throws IOException, DatabaseFormatException {
		currentData.addRecord(question, answer+"", input+"");
	}
	
	public Table loadDataFromDate(int day, int month, int year, java.awt.Dialog parent) throws FileNotFoundException, IOException, DatabaseFormatException {
		Table data = new Table(0, 3);
		data.setBackground(Color.white);
		data.setForeground(Color.black);
		data.setColumnHeaderVisible(true);
		data.setRowHeaderVisible(false);
		data.setHeaderBackground(Color.black);
		data.setHeaderForeground(Color.white);
		data.setColumnHeaders("Question", "Answer", "Input");
		
		RecordDatabase x = null;
		
		userData.reset();
		
		String[] line;
		
		long size = 0, count = 0;
		
		final ProgressBar progress1;
		
		if (DataIO.DAY == day && DataIO.MONTH == month && DataIO.YEAR == year) {
			currentData.reset();
			x = currentData;
			size = currentData.length();
		} else {
			size = userData.length();
			userData.reset();
			progress1 = new ProgressBar("Searching for data from "+month+"/"+day+"/"+year+"...", parent);
			while (userData.hasNext()) {
				line = userData.next();
				progress1.setPercentValue((int) (count++ * 100 / size));
				if (line[0].equals(""+day) && line[1].equals(""+month) && line[2].equals(""+year)) {
					File f = new File(DataIO.APP_DATA+"/"+line[3]);
					progress1.setTitle("Extracting data...");
					RecordDatabase.setListener(new StatusListener() {
						@Override
						public void updateProgress(double p) {
							progress1.setPercentValue((int) p);
						}
					});
					x = RecordDatabase.loadEncryptedDatabase(f,  WORKER);
					size = x.length();
					break;
				}
				
			}
			progress1.dispose();
			if (x == null) return data;
		}
		
		ProgressBar progress2 = new ProgressBar("Loading...", parent);
		
		count = 0;
		
		while (x.hasNext()) {
			line = x.next();
			progress2.setPercentValue((int) (count++ * 100 / size));
			data.addRow();
			data.setValue(line[0], data.getLastRow(), 1);
			data.setValue(line[1], data.getLastRow(), 2);
			data.setValue(line[2], data.getLastRow(), 3);
			if (line[1].equals(line[2])) data.setBackground(data.getLastRow(), Color.green);
			else data.setBackground(data.getLastRow(), Color.red);
		}
		progress2.dispose();
		progress2 = null;
		
		if (!(DataIO.DAY == day && DataIO.MONTH == month && DataIO.YEAR == year)) x.close();
		return data;
	}
	
	public Table loadData(java.awt.Dialog parent) throws FileNotFoundException, IOException, DatabaseFormatException {
		
		Table data = new Table(0, 4);
		data.setBackground(Color.white);
		data.setForeground(Color.black);
		data.setColumnHeaderVisible(true);
		data.setRowHeaderVisible(false);
		data.setHeaderBackground(Color.black);
		data.setHeaderForeground(Color.white);
		data.setColumnHeaders("Date", "Right", "Wrong", "Score");
		
		String[] line;
		
		RecordDatabase x;
		
		int right, wrong, score;
		boolean today;
		
		currentData.reset();
		userData.reset();
		
		File file;
		
		long size = 0, count = 0, size2 = userData.length();
		
		ProgressBar progress = new ProgressBar("Calculating size...", parent);
		
		while (userData.hasNext()) {
			progress.setPercentValue((int) (count++ * 100 / size2));
			size += Long.parseLong(userData.next()[4]);
		}
		
		count = 0;
		
		userData.reset();
		
		progress.setTitle("Loading...");
		
		while (userData.hasNext()) {
			line = userData.next();
			data.addRow();
			data.setValue(line[1]+"/"+line[0]+"/"+line[2], data.getLastRow(), 1);
			today = line[0].equals(""+DAY) && line[1].equals(""+MONTH) && line[2].equals(""+YEAR);
			//true if today
			
			file = new File(APP_DATA+"/"+line[3]);
			//javax.swing.JOptionPane.showMessageDialog(null, line[3]+" "+file.exists());
			
			x = today ?
					currentData :
						RecordDatabase.loadEncryptedDatabase(file, WORKER);
			
			//if today, use the database which is already open
			
			score = right = wrong = 0;
			
			while (x.hasNext()) {
				line = x.next();

				progress.setPercentValue(Math.round(count++ * 100 / (size == 0 ? 1 : size)));
				
				if (line[1].equals(line[2])) {
					right++;
				} else {
					wrong++;
				}
			}
			
			score = right + wrong == 0 ? 0 : (int)Math.round(100 * (double)right / (double)(right + wrong));
			if (!today) x.close();
			x = null;
			if (right + wrong == 0) {
				data.removeRow(data.getLastRow());
				continue;
			}
			data.setValue(String.valueOf(right), data.getLastRow(), 2);
			data.setValue(wrong+"", data.getLastRow(), 3);
			data.setValue(score+"%", data.getLastRow(), 4);
			data.setBackground(data.getLastRow(), score >= 90 ? Color.green : score >=70 ? Color.yellow : Color.red);
		}
		
		progress.dispose();
		progress = null;
		
		return data;
	}
	
	private static String stringifyColor(Color c) {
		//returns parsable string representation of color
		//depreciated version of stringify: return "{r:"+c.getRed()+",g:"+c.getGreen()+",b:"+c.getBlue()+"}";
		String r = Integer.toString(c.getRed(), 16), g = Integer.toString(c.getGreen(), 16), b = Integer.toString(c.getBlue(), 16);
		return "#"
				+(r.length() == 2 ? r : (r.length() == 1 ? "0"+r : "00"))
				+(g.length() == 2 ? g : (g.length() == 1 ? "0"+g : "00"))
				+(b.length() == 2 ? b : (b.length() == 1 ? "0"+b : "00"));
	}
	
	public static Color parseColor(String t) {
		//returns null if invalid string
		if (t.matches("\\{r:[0-9]{1,},g:[0-9]{1,},b:[0-9]{1,}\\}")) {
			//still provide support for depreciated stringify version
			String[] s = t.replaceAll("\\{", "").replaceAll("\\}", "").split(",");
			return new Color(Integer.parseInt(s[0].replaceAll("r:", "")),Integer.parseInt(s[1].replaceAll("g:", "")),Integer.parseInt(s[2].replaceAll("b:", "")));
		} else if (t.matches("\\#[A-Fa-f0-9]{6,6}")) {
			t = t.replaceAll("\\#", "");
			return new Color(Integer.parseInt(t.substring(0, 2), 16), Integer.parseInt(t.substring(2, 4), 16), Integer.parseInt(t.substring(4, 6), 16));
		} else {
			return null;
		}
	}
	
	private static String generateCode() {
		String r = "";
		int i;
		for ( i=0 ; i<3 ; i++ ) {
			r += (char) (RANDOM.nextInt(26) + 'A');
		}
		for ( i=0 ; i<3 ; i++ ) {
			r += RANDOM.nextInt(10);
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += (char) (RANDOM.nextInt(26) + 'A');
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += RANDOM.nextInt(10);
		}
		for ( i=0 ; i<3 ; i++ ) {
			r += (char) (RANDOM.nextInt(26) + 'A');
		}
		for ( i=0 ; i<3 ; i++ ) {
			r += RANDOM.nextInt(10);
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += (char) (RANDOM.nextInt(26) + 'A');
		}
		for ( i=0 ; i<5 ; i++ ) {
			r += RANDOM.nextInt(10);
		}
		return r;
	}
	
	public void logout() throws IOException, DatabaseFormatException {
		userData.reset();
		String[] line;
		while (userData.hasNext()) {
			line = userData.next();
			if (line[0].equalsIgnoreCase(""+DAY) && line[1].equalsIgnoreCase(""+MONTH) && line[2].equalsIgnoreCase(""+YEAR)) {
				userData.editRecord(line, 
						DAY+"", MONTH+"", YEAR+"", line[3], currentData.length()+"");
				break;
			}
		}
		currentData.close();
		userData.close();
	}
}