package hansedwardhoene.app.practicemath;

import hansedwardhoene.cryptography.CryptographyByteManager;
import hansedwardhoene.cryptography.DecryptedInputStream;
import hansedwardhoene.cryptography.EncryptedOutputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class QueryDatabase implements Iterator<Query>, Iterable<Query>, Closeable, AutoCloseable {
	//written by Hans-Edward Hoene
	
	private DecryptedInputStream in;
	private File tmp;
	private Query nextQuery;
	private long length;
	
	private ArrayList<StatusListener> listeners = new ArrayList<StatusListener>();
	
	private final static CryptographyByteManager CRYPTOGRAPHY_WORKER = new CryptographyByteManager(16, 5, 53, 299, true);
	
	public void addListener(StatusListener l) {
		listeners.add(l);
	}
	
	public void removeListener(int index) {
		listeners.remove(index);
	}
	
	public boolean removeListener(StatusListener l) {
		return listeners.remove(l);
	}
	
	public void removeAllListeners() {
		listeners.clear();
	}
	
	@Override
	public boolean hasNext() {
		//only hasNext when the next query does not equal null
		return nextQuery != null;
	}

	@Override
	public Query next() {
		Query r;//r is return value
		if (nextQuery != null) {
			r = nextQuery;
		} else {
			//if no next query, reset and go back to beginning
			reset();
			r = nextQuery;
		}
		nextQuery = getQuery();//set nextQuery to the next query
		return r;
	}
	
	public long length() {
		return length;
	}
	
	public static QueryDatabase parse(InputStream in, StatusListener listener) throws IOException {
		return new QueryDatabase(in, listener);
	}
	
	private QueryDatabase(InputStream in, StatusListener listener) throws IOException {
		//length is only used for progress bar
		new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/").mkdirs();
		do {
			tmp = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/PracticeMath_Question_Database"+Math.random()+".tmp");
		} while (tmp.exists());
		tmp.createNewFile();
		if (listener != null) addListener(listener);
		EncryptedOutputStream fos = new EncryptedOutputStream(new FileOutputStream(tmp), CRYPTOGRAPHY_WORKER);
		int next = in.read();
		long count = 0;
		long queryCount = 0;
		while (next != -1) {
			fos.write(next);
			if ((char)(next & 0xFF) == '&') queryCount++;
			if (++count % 100 == 0) updateProgress(count);
			next = in.read();
		}
		fos.close();
		in.close();
		this.length = queryCount;
		this.in = new DecryptedInputStream(new FileInputStream(tmp), CRYPTOGRAPHY_WORKER);
		nextQuery = getQuery();
	}
	
	private QueryDatabase(InputStream in) throws IOException {
		this(in,null);
	}
	
	public void reset() {
		try {
			in.close();
		} catch (IOException e) {}
		try {
			this.in = new DecryptedInputStream(new FileInputStream(tmp), CRYPTOGRAPHY_WORKER);
			nextQuery = getQuery();
		} catch (FileNotFoundException e) {
			throw new InternalError("An error with the database has ocurred!");
		}
	}
	
	private Query getQuery() {
		
		int next;
		char c;
		try {
			next = in.read();
		} catch (IOException e) {
			return null;
		}
		c = (char)(next & 0xFF);
		if (next == -1) {
			return null;
		} else if (c == '\n' || c == '&') {
			return getQuery();
		}
		String ret = "";
		while (next != -1 && c != '\n' && c != '&') {
			ret += c;
			try {
				next = in.read();
			} catch (IOException e) {
				int i = ret.indexOf('=');
				return new Query(ret.substring(0, i), ret.substring(i + 1, ret.length()));
			}
			c = (char)(next & 0xFF);
		}
		int i = ret.indexOf('=');
		if (i == -1) return new Query(ret, "");
		else return new Query(ret.substring(0, i), ret.substring(i + 1, ret.length()));
	}
	
	@Override
	public Iterator<Query> iterator() {
		return this;
	}

	@Override
	public void close() throws IOException {
		IOException err = null;
		try {
			in.close();
		} catch (IOException e) {
			err = e;
		}
		tmp.delete();
		if (err != null) throw err;
	}
	
	private void updateProgress(double p) {
		(new Thread() {
			public void run() {
				for ( int i=0 ; i<listeners.size() ; i++ ) {
					try {
						listeners.get(i).updateProgress(p);
					} catch (Throwable e) {}
				}
			}
		}).start();
	}
}