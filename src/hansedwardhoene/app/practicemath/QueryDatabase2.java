package hansedwardhoene.app.practicemath;

import hansedwardhoene.cryptography.*;

import java.io.*;
import java.util.Iterator;
import java.util.Random;

public class QueryDatabase2 {//implements Iterator<Query>, Iterable<Query>, Closeable, AutoCloseable {
	/*
	private File data;
	private DecryptedInputStream in;
	private Query nextQuery;
	
	private static final int[][] CRYPTOGRAPHY_INTS = {
		{5, 53, 299},
	};
	
	private final CryptographyByteManager CRYPTOGRAPHY_WORKER;
	
	public QueryDatabase2() {
		Random random = new Random();
		int[] keys = CRYPTOGRAPHY_INTS[random.nextInt(CRYPTOGRAPHY_INTS.length)];
		CRYPTOGRAPHY_WORKER = new CryptographyByteManager(random.nextBoolean() ? (random.nextBoolean() ? CryptographyByteManager.BINARY : CryptographyByteManager.OCTAL) : (random.nextBoolean() ? CryptographyByteManager.BASE_TEN : CryptographyByteManager.HEXADECIMAL), keys[0], keys[1], keys[2], true);
		new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/").mkdirs();
		data = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/PracticeMath_Question_Database"+Math.random()+".tmp");
		try {
			in = new DecryptedInputStream(new FileInputStream(data), CRYPTOGRAPHY_WORKER);
		} catch (FileNotFoundException e) {throw new InternalError();}
		nextQuery = getNextQuery();
	}
	
	public QueryDatabase2(InputStream source) throws IOException {
		Random random = new Random();
		int[] keys = CRYPTOGRAPHY_INTS[random.nextInt(CRYPTOGRAPHY_INTS.length)];
		CRYPTOGRAPHY_WORKER = new CryptographyByteManager(random.nextBoolean() ? (random.nextBoolean() ? CryptographyByteManager.BINARY : CryptographyByteManager.OCTAL) : (random.nextBoolean() ? CryptographyByteManager.BASE_TEN : CryptographyByteManager.HEXADECIMAL), keys[0], keys[1], keys[2], true);
		new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/").mkdirs();
		data = new File(System.getenv("USERPROFILE")+"/Hoene Multipurpose Software Development Company/Temporary Files/PracticeMath_Question_Database"+Math.random()+".tmp");
		EncryptedOutputStream out = new EncryptedOutputStream(new FileOutputStream(data), CRYPTOGRAPHY_WORKER);
		int next = source.read();
		while (next != -1) {
			out.write(next);
			next = source.read();
		}
		out.close();
		in = new DecryptedInputStream(new FileInputStream(data), CRYPTOGRAPHY_WORKER);
		nextQuery = getNextQuery();
	}
	
	private Query getNextQuery() {
		int next;
		char c;
		try {
			next = in.read();
		} catch (IOException e) {
			return null;
		}
		c = (char)(next & 0xFF);
		if (next == -1) {
			return null;
		} else if (c == '\n' || c == '&') {
			return getNextQuery();
		}
		String ret = "";
		while (next != -1 && c != '\n' && c != '&') {
			ret += c;
			try {
				next = in.read();
			} catch (IOException e) {
				int i = ret.indexOf('=');
				return new Query(ret.substring(0, i), ret.substring(i + 1, ret.length()));
			}
			c = (char)(next & 0xFF);
		}
		int i = ret.indexOf('=');
		if (i == -1) return new Query(ret, "");
		else return new Query(ret.substring(0, i), ret.substring(i + 1, ret.length()));
	}

	@Override
	public void close() throws IOException {
		IOException err = null;
		try {
			in.close();
		} catch (IOException e) {
			err = e;
		}
		data.delete();
		if (err != null) throw err;
	}

	@Override
	public Iterator<Query> iterator() {
		reset();
		return this;
	}

	@Override
	public boolean hasNext() {
		return nextQuery != null;
	}

	@Override
	public Query next() {
		Query r = nextQuery;
		nextQuery = getNextQuery();
		return r;
	}
	
	public void reset() {
		try {
			in.close();
		} catch (IOException e) {}
		try {
			this.in = new DecryptedInputStream(new FileInputStream(data), CRYPTOGRAPHY_WORKER);
			nextQuery = getNextQuery();
		} catch (FileNotFoundException e) {
			throw new InternalError("An error with the database has ocurred!");
		}
	}
	
	public void addQuery(Query q) throws IOException {
		in.close();
		EncryptedOutputStream out = new EncryptedOutputStream(new FileOutputStream(data, true), CRYPTOGRAPHY_WORKER);
		out.write('&'+q.toString());
		out.close();
		reset();
	}
	
	public void load(OutputStream out) throws IOException {
		in.close();
		in = new DecryptedInputStream(new FileInputStream(data), CRYPTOGRAPHY_WORKER);
		int next;
		while ((next = in.read()) != -1) {
			out.write(next);
		}
		in.close();
		out.close();
		reset();
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		File f = new File("C:/Users/hansh/desktop/my_test.txt");
		if (!f.exists()) f.createNewFile();
		QueryDatabase2 x = new QueryDatabase2(new FileInputStream(f));
		ProgressBar p = ProgressBar.showProgressBar("hey");
		int i = 0;
		while (x.hasNext()) {
			p.setPercentValue((int) ((i+=x.next().toString().length() + 1) * x.CRYPTOGRAPHY_WORKER.getNumberLength() / x.data.length()));
			//System.out.println(x.next());
		}
		System.out.println();
		x.reset();
		x.load(new FileOutputStream(f));
		x.close();
	}
	*/
}