package hansedwardhoene.app.practicemath;

import java.awt.Color;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;

public class Table extends java.awt.ScrollPane implements java.awt.event.MouseListener {
	//written by Hans-Edward Hoene

	private static final long serialVersionUID = 1L;
	
	private Panel pane;
	private JLabel[][] data;
	
	private int rows, cols, rowSize, colSize;
	
	private Border border;
	private Color background, foreground, headerbackground, headerforeground;
	private Font font, headerFont;
	
	private ArrayList<TableListener> listeners;
	
	private boolean colHeaderVisible, rowHeaderVisible;
	
	public Table(int rows, int cols) {
		super();
		
		data = new JLabel[rows + 1][cols + 1];
		this.rows = rows;this.cols = cols;
		this.colHeaderVisible = this.rowHeaderVisible = true;
		rowSize = colSize = 0;
		pane = new Panel();
		pane.setLayout(null);
		
		background = Color.white;
		foreground = Color.black;
		headerbackground = Color.black;
		headerforeground = Color.white;
		border = BorderFactory.createLineBorder(foreground, 1);
		font = super.getFont();
		headerFont = super.getFont();
		
		int i, j;
		
		for ( i=0 ; i<rows + 1 ; i++ ) {
			for ( j=0 ; j<cols + 1 ; j++ ) {
				data[i][j] = null;
			}
		}
		
		resetCells();
		
		listeners = new ArrayList<TableListener>();
		
		super.add(pane);
	}
	
	private void resetCells() {
		int i, j;
		pane.removeAll();
		for ( i=0 ; i<rows + 1 ; i++ ) {
			for ( j=0 ; j<cols + 1 ; j++ ) {
				if (data[i][j] == null) {
					data[i][j] = new JLabel();
					data[i][j].setBorder(border);
					data[i][j].setBackground(i == 0 || j == 0 ? headerbackground : background);
					data[i][j].setForeground(i == 0 || j == 0 ? headerforeground : foreground);
					data[i][j].setFont(i == 0 || j == 0 ? headerFont : font);
					data[i][j].setOpaque(true);
					data[i][j].addMouseListener(this);
				}
				data[i][j].setSize(colSize, rowSize);
				data[i][j].setLocation((this.rowHeaderVisible ? j : j - 1) * colSize, (this.colHeaderVisible ? i : i - 1) * rowSize);
				//data[i][j].invalidate();
				//data[i][j].revalidate();
				//data[i][j].repaint();
				pane.add(data[i][j]);
			}
		}
		data[0][0].setText("X");
		pane.invalidate();
		pane.revalidate();
		pane.repaint();
		super.invalidate();
		super.revalidate();
		super.repaint();
	}
	
	public void setRowSize(int size) {
		this.rowSize = size;
		pane.setSize(this.colSize * cols, this.rowSize * rows);
		resetCells();
	}
	
	public void setColSize(int size) {
		this.colSize = size;
		pane.setSize(this.colSize * cols, this.rowSize * rows);
		resetCells();
	}
	
	public void setCellSize(int rowSize, int colSize) {
		this.rowSize = rowSize;
		this.colSize = colSize;
		pane.setSize(this.colSize * cols, this.rowSize * rows);
		resetCells();
	}
	
	public void addRow() {
		JLabel[][] copy = new JLabel[rows + 2][cols + 1];
		int r, c;
		for ( r=0 ; r<rows + 1 ; r++ ) {
			for ( c=0 ; c<cols + 1 ; c++ ) {
				copy[r][c] = data[r][c];
			}
		}
		r = rows++ + 1;//last row then add one
		for ( c=0 ; c<cols + 1 ; c++ ) {
			copy[r][c] = null;
		}
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void addColumn() {
		JLabel[][] copy = new JLabel[rows + 1][cols + 2];
		int r, c;
		for ( r=0 ; r<rows + 1 ; r++ ) {
			for ( c=0 ; c<cols + 1 ; c++ ) {
				copy[r][c] = data[r][c];
			}
		}
		c = cols++ + 1;//last row then add one
		for ( r=0 ; r<rows + 1 ; r++ ) {
			copy[r][c] = null;
		}
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void removeRow(int row) {
		if (row <= 0 || row >= rows + 1) throw new ArrayIndexOutOfBoundsException();
		JLabel[][] copy = new JLabel[rows][cols + 1];//rows is already one less than the array
		pane.removeAll();
		int r, c;
		for ( r=0 ; r<rows+1 ; r++ ) {
			if (r == row) continue;
			for ( c=0 ; c<cols + 1 ; c++ ) {
				copy[r > row ? r - 1 : r][c] = data[r][c];
				pane.add(copy[r > row ? r - 1 : r][c]);
			}
		}
		rows--;
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void removeColumn(int col) {
		if (col <=0 || col >= rows + 1) throw new ArrayIndexOutOfBoundsException();
		JLabel[][] copy = new JLabel[rows + 1][cols];//rows is already one less than the array
		pane.removeAll();
		int r, c;
		for ( r=0 ; r<rows + 1 ; r++ ) {
			for ( c=0 ; c<cols + 1 ; c++ ) {
				if (c == col) continue;
				copy[r][c > col ? c - 1 : c] = data[r][c];
				pane.add(copy[r][c > col ? c - 1 : c]);
			}
		}
		cols--;
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void setValue(String value, int row, int col) {
		data[row][col].setText(value);
		LISTENER_cellValueChanged(value, row, col);
	}
	
	public String getValue(int row, int col) {
		return data[row][col].getText();
	}
	
	public void clearAllCells() {
		int r, c;
		for ( r=1 ; r<=rows + 1 ; r++ ) {
			for ( c=1 ; c<=cols + 1 ; c++ ) {
				setValue("", r, c);
			}
		}
	}
	
	public int getLastRow() {
		return rows;
	}
	
	public int getLastColumn() {
		return cols;
	}
	
	public void addTableListener(TableListener l) {
		this.listeners.add(l);
	}
	
	public boolean removeTableListener(TableListener l) {
		return this.listeners.remove(l);
	}
	
	public void removeTableListener(int index) {
		this.listeners.remove(index);
	}
	
	public void removeAllTableListeners() {
		this.listeners.clear();
	}
	
	public void setColumnHeaders(String...headers) {
		for ( int i=1 ; i<=headers.length && i<cols+1 ; i++ ) {
			data[0][i].setText(headers[i - 1]);
		}
	}
	
	public String[] getColumnHeaders() {
		String[] ret = new String[cols];
		for ( int i=1 ; i<cols + 1 ; i++ ) {
			ret[i] = data[0][i].getText();
		}
		return ret;
	}
	
	public void setRowHeaders(String...headers) {
		for ( int i=1 ; i<=headers.length && i<rows+1 ; i++ ) {
			data[i][0].setText(headers[i - 1]);
		}
	}
	
	public String[] getRowHeaders() {
		String[] ret = new String[rows];
		for ( int i=1 ; i<rows + 1 ; i++ ) {
			ret[i] = data[i][0].getText();
		}
		return ret;
	}
	
	@Override
	public void setBackground(Color b) {
		this.background = b;
		int i, j;
		for ( i=0 ; i<rows+1 ; i++ ) {
			for ( j=0 ; j<cols+1 ; j++ ) {
				data[i][j].setBackground(this.background);
			}
		}
	}
	
	@Override
	public Color getBackground() {
		return this.background;
	}
	
	@Override
	public void setForeground(Color f) {
		this.foreground = f;
		int i, j;
		for ( i=0 ; i<rows+1 ; i++ ) {
			for ( j=0 ; j<cols+1 ; j++ ) {
				data[i][j].setForeground(this.foreground);
			}
		}
	}
	
	@Override
	public Color getForeground() {
		return this.foreground;
	}
	
	public void setHeaderBackground(Color b) {
		this.headerbackground = b;
		int i;
		for ( i=0 ; i<rows+1 ; i++ ) {
			data[i][0].setBackground(this.headerbackground);
		}
		for ( i=0 ; i<cols+1 ; i++ ) {
			data[0][i].setBackground(this.headerbackground);
		}
	}
	
	public Color getHeaderBackground() {
		return this.headerbackground;
	}
	
	public void setHeaderForeground(Color f) {
		this.headerforeground = f;
		int i;
		for ( i=0 ; i<rows+1 ; i++ ) {
			data[i][0].setForeground(this.headerforeground);
		}
		for ( i=0 ; i<cols+1 ; i++ ) {
			data[0][i].setForeground(this.headerforeground);
		}
	}
	
	public Color getHeaderForeground() {
		return this.headerforeground;
	}
	
	public void setBorder(Border b) {
		this.border = b;
		int i, j;
		for ( i=0 ; i<rows+1 ; i++ ) {
			for ( j=0 ; j<cols+1 ; j++ ) {
				data[i][j].setBorder(this.border);
			}
		}
	}
	
	public Border getBorder() {
		return this.border;
	}
	
	@Override
	public void setFont(Font f) {
		this.font = f;
		int i, j;
		for ( i=0 ; i<rows+1 ; i++ ) {
			for ( j=0 ; j<cols+1 ; j++ ) {
				data[i][j].setFont(this.font);
			}
		}
	}
	
	@Override
	public Font getFont() {
		return this.font;
	}
	
	public void setHeaderFont(Font f) {
		this.headerFont = f;
		int i;
		for ( i=0 ; i<rows+1 ; i++ ) {
			data[i][0].setFont(this.headerFont);
		}
		for ( i=0 ; i<cols+1 ; i++ ) {
			data[0][i].setFont(this.headerFont);
		}
	}
	
	public Font getHeaderFont() {
		return this.headerFont;
	}
	
	public void setRowHeaderVisible(boolean visible) {
		int i;
		this.rowHeaderVisible = visible;
		if (this.rowHeaderVisible) {
			for ( i=0 ; i<rows + 1 ; i++ ) {
				pane.add(data[i][0]);
			}
		} else {
			for ( i=0 ; i<rows + 1 ; i++ ) {
				pane.remove(data[i][0]);
			}
		}
		resetCells();
	}
	
	public void setColumnHeaderVisible(boolean visible) {
		int i;
		this.colHeaderVisible = visible;
		if (this.colHeaderVisible) {
			for ( i=0 ; i<cols + 1 ; i++ ) {
				pane.add(data[0][i]);
			}
		} else {
			for ( i=0 ; i<cols + 1 ; i++ ) {
				pane.remove(data[0][i]);
			}
		}
		resetCells();
	}
	
	public void setHeaderVisible(boolean visible) {
		setRowHeaderVisible(visible);
		setColumnHeaderVisible(visible);
	}
	
	public void setCornerHeaderCellText(String txt) {
		data[0][0].setText(txt);
	}
	
	public String getCornerHeaderCellText() {
		return data[0][0].getText();
	}
	
	private void LISTENER_cellClicked(MouseEvent e, int row, int col) {
		(new Thread() {
			public void run() {
				for ( int i=0 ; i<listeners.size() ; i++ ) {
					listeners.get(i).cellClicked(e, row, col);
				}
			}
		}).start();
	}
	
	private void LISTENER_cellValueChanged(String newVal, int row, int col) {
		(new Thread() {
			public void run() {
				for ( int i=0 ; i<listeners.size() ; i++ ) {
					listeners.get(i).cellValueChanged(newVal, row, col);
				}
			}
		}).start();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		int i, j;
		Object target = e.getSource();
		for ( i=0 ; i<rows + 1 ; i++ ) {
			for ( j=0 ; j<cols + 1 ; j++ ) {
				if (target == data[i][j]) {
					LISTENER_cellClicked(e, i, j);
					return;
				}
			}
		}
	}
	
	public void setBackground(int row, Color c) {
		for ( int i=1 ; i<cols+1 ; i++ ) {
			data[row][i].setBackground(c);
		}
	}

	@Override public void mouseEntered(MouseEvent e) {}

	@Override public void mouseExited(MouseEvent e) {}

	@Override public void mousePressed(MouseEvent e) {}

	@Override public void mouseReleased(MouseEvent e) {}
}

/*public class Table extends java.awt.ScrollPane {

	private static final long serialVersionUID = 1L;
	
	private Panel pane;
	private JLabel[][] data;
	
	private int rows, cols, rowSize, colSize;
	
	private Border border;
	private Color background, foreground, headerbackground, headerforeground;
	private Font font;
	
	private ArrayList<TableListener> listeners;
	private MouseAdapter clickEventHandler;
	
	public Table(int rows, int cols, String ... headers) {
		super();
		
		data = new JLabel[rows + 1][cols];
		this.rows = rows;this.cols = cols;
		rowSize = colSize = 0;
		pane = new Panel();
		pane.setLayout(null);
		
		background = Color.white;
		foreground = Color.black;
		headerbackground = Color.black;
		headerforeground = Color.white;
		border = BorderFactory.createLineBorder(foreground, 1);
		font = new Font("Arial", 0, 12);
		clickEventHandler = (new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i, j;
				Object target = e.getSource();
				for ( i=0 ; i<rows + 1 ; i++ ) {
					for ( j=0 ; j<cols ; j++ ) {
						if (target == data[i][j]) {
							LISTENER_cellClicked(e, i, j + 1);
							return;
						}
					}
				}
			}
		});
		
		int i, j;
		
		for ( i=0 ; i<rows + 1 ; i++ ) {
			for ( j=0 ; j<cols ; j++ ) {
				data[i][j] = new JLabel();
				data[i][j].setBorder(border);
				data[i][j].setBackground(i == 0 ? headerbackground : background);
				data[i][j].setForeground(i == 0 ? headerforeground : foreground);
				data[i][j].setFont(font);
				data[i][j].setSize(0, 0);
				data[i][j].setLocation(0, 0);
				data[i][j].addMouseListener(clickEventHandler);
				pane.add(data[i][j]);
			}
		}
		
		for ( i=0 ; i<headers.length && i<cols ; i++ ) {
			data[0][i].setText(headers[i]);
		}
		
		
		listeners = new ArrayList<TableListener>();
		
		super.add(pane);
	}
	
	private void resetCells() {
		int i, j;
		for ( i=0 ; i<rows + 1 ; i++ ) {
			for ( j=0 ; j<cols ; j++ ) {
				data[i][j].setBorder(border);
				data[i][j].setBackground(i == 0 ? headerbackground : background);
				data[i][j].setForeground(i == 0 ? headerforeground : foreground);
				data[i][j].setFont(font);
				data[i][j].setSize(colSize, rowSize);
				data[i][j].setLocation(j * colSize, i * rowSize);
				data[i][j].setOpaque(true);
				data[i][j].addMouseListener(clickEventHandler);
			}
		}
		super.revalidate();
		super.repaint();
	}
	
	public void setRowSize(int size) {
		this.rowSize = size;
		pane.setSize(this.colSize * cols, this.rowSize * rows);
		resetCells();
	}
	
	public void setColSize(int size) {
		this.colSize = size;
		pane.setSize(this.colSize * cols, this.rowSize * rows);
		resetCells();
	}
	
	public void setCellSize(int rowSize, int colSize) {
		this.rowSize = rowSize;
		this.colSize = colSize;
		pane.setSize(this.colSize * cols, this.rowSize * rows);
		resetCells();
	}
	
	public void addRow() {
		JLabel[][] copy = new JLabel[rows + 2][cols];
		int r, c;
		for ( r=0 ; r<rows + 1 ; r++ ) {
			for ( c=0 ; c<cols ; c++ ) {
				copy[r][c] = data[r][c];
			}
		}
		r = rows++ + 1;//last row then add one
		for ( c=0 ; c<cols ; c++ ) {
			copy[r][c] = new JLabel();
			pane.add(copy[r][c]);
		}
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void addCol() {
		JLabel[][] copy = new JLabel[rows + 1][cols + 1];
		int r, c;
		for ( r=0 ; r<rows + 1 ; r++ ) {
			for ( c=0 ; c<cols ; c++ ) {
				copy[r][c] = data[r][c];
			}
		}
		c = cols++;//last col and add one
		for ( r=0 ; r<rows+1 ; r++ ) {
			copy[r][c] = new JLabel();
			pane.add(copy[r][c]);
		}
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void removeRow(int row) {
		if (row < 0 || row >= rows + 1) throw new ArrayIndexOutOfBoundsException();
		JLabel[][] copy = new JLabel[rows][cols];//rows is already one less than the array
		pane.removeAll();
		int r, c;
		for ( r=0 ; r<rows+1 ; r++ ) {
			if (r == row) continue;
			for ( c=0 ; c<cols ; c++ ) {
				copy[r > row ? r - 1 : r][c] = data[r][c];
				pane.add(copy[r > row ? r - 1 : r][c]);
			}
		}
		rows--;
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void removeCol(int col) {
		if (col < 0 || col >= cols) throw new ArrayIndexOutOfBoundsException();
		col--;
		JLabel[][] copy = new JLabel[rows + 1][--cols];
		pane.removeAll();
		int r, c;
		for ( r=0 ; r<rows+1 ; r++ ) {
			for ( c=0 ; c<(cols + 1) ; c++ ) {
				if (c == col) continue;
				copy[r][c > col ? c - 1 : c] = data[r][c];
				pane.add(copy[r][c > col ? c - 1 : c]);
			}
		}
		data = copy;
		copy = null;
		resetCells();
	}
	
	public void setValue(String value, int row, int col) {
		data[row][col - 1].setText(value);
		LISTENER_cellValueChanged(value, row, col);
	}
	
	public String getValue(int row, int col) {
		return data[row][col - 1].getText();
	}
	
	public void clearAllCells() {
		int r, c;
		for ( r=1 ; r<=rows + 1 ; r++ ) {
			for ( c=1 ; c<=cols ; c++ ) {
				setValue("", r, c);
			}
		}
	}
	
	public int getLastRow() {
		return rows;
	}
	
	public int getLastColumn() {
		return cols;
	}
	
	public void addTableListener(TableListener l) {
		this.listeners.add(l);
	}
	
	public boolean removeTableListener(TableListener l) {
		return this.listeners.remove(l);
	}
	
	public void removeTableListener(int index) {
		this.listeners.remove(index);
	}
	
	public void removeAllTableListeners() {
		this.listeners.clear();
	}
	
	public void setHeaders(String...headers) {
		for ( int i=0 ; i<headers.length && i<cols ; i++ ) {
			data[0][i].setText(headers[i]);
		}
	}
	
	public String[] getHeaders() {
		String[] ret = new String[cols];
		for ( int i=0 ; i<cols ; i++ ) {
			ret[i] = data[0][i].getText();
		}
		return ret;
	}
	
	/*@Override
	public void setSize(int width, int height) {
		super.setSize(width, height);
		resetCells();
	}
	
	@Override
	public void setBackground(Color b) {
		this.background = b;
		resetCells();
	}
	
	@Override
	public Color getBackground() {
		return this.background;
	}
	
	@Override
	public void setForeground(Color f) {
		this.foreground = f;
		resetCells();
	}
	
	@Override
	public Color getForeground() {
		return this.foreground;
	}
	
	public void setHeaderBackground(Color b) {
		this.headerbackground = b;
		resetCells();
	}
	
	public Color getHeaderBackground() {
		return this.headerbackground;
	}
	
	public void setHeaderForeground(Color f) {
		this.headerforeground = f;
		resetCells();
	}
	
	public Color getHeaderForeground() {
		return this.headerforeground;
	}
	
	public void setBorder(Border b) {
		this.border = b;
		resetCells();
	}
	
	public Border getBorder() {
		return this.border;
	}
	
	private void LISTENER_cellClicked(MouseEvent e, int row, int col) {
		(new Thread() {
			public void run() {
				for ( int i=0 ; i<listeners.size() ; i++ ) {
					listeners.get(i).cellClicked(e, row, col);
				}
			}
		}).start();
	}
	
	private void LISTENER_cellValueChanged(String newVal, int row, int col) {
		(new Thread() {
			public void run() {
				for ( int i=0 ; i<listeners.size() ; i++ ) {
					listeners.get(i).cellValueChanged(newVal, row, col);
				}
			}
		}).start();
	}
}*/