package hansedwardhoene;

/*filename: Array.java*/
@SuppressWarnings("unchecked")
public class Array<T> {
	private T[] data;
	private T[] temp;
	
	public Array() {
		data = (T[])new Object[0];
	}
	
	public Array(T...elements) {
		data = (T[])new Object[0];
		for ( int i=0 ; i<elements.length ; i++ ) {
			add(elements[i]);
		}
	}

	public void add(T element) {
		//adds element to array
		temp = (T[])new Object[data.length + 1];
		for ( int i=0 ; i<data.length ; i++ ) {
			temp[i] = data[i];
		}
		temp[data.length] = element;
		data = temp;
		temp = null;
	}
	
	public void add(T...elements) {
		//adds elements to array
		for ( int i=0 ; i<elements.length ; i++ ) {
			add(elements[i]);
		}
	}
	
	public T get(int index) {
		//returns element from array
		return data[index];
	}
	
	public void remove(int index) throws ArrayIndexOutOfBoundsException {
		//removes element from array by index
		if (index >= (data.length)) {
			throw new ArrayIndexOutOfBoundsException("Index is greater than highest element in array.");
		}
		
		temp = (T[])new Object[data.length - 1];
		for ( int i=0 ; i<data.length ; i++ ) {
			if (i < index) {
				temp[i] = data[i];
			} else if (i == index) {
				continue;
			} else {
				temp[i - 1] = data[i];
			}
		}
		data = temp;
		temp = null;
	}
	
	public boolean remove(T element) {
		//removes element from array by object reference
		for ( int i=0 ; i<data.length ; i++ ) {
			if (data[i] == element) {
				remove(i);
				return true;
			}
		}
		for ( int i=0 ; i<data.length ; i++ ) {
			if (data[i].equals(element)) {
				remove(i);
				return true;
			}
		}
		return false;
	}
	
	public int getLength() {
		//returns array length
		return data.length;
	}
	
	public void clear() {
		//clears array
		for ( int i=0 ; i<data.length ; i++ ) {
			data[i] = null;
		}
		data = (T[])new Object[0];
	}
	
	public T pop() {
		//removes last element from array and returns it
		temp = (T[])new Object[data.length - 1];
		for ( int i=0 ; i<temp.length ; i++ ) {
			temp[i] = data[i];
		}
		T ret = data[temp.length];
		data = temp;
		temp = null;
		return ret;
	}
	
	public boolean swap(int index1, int index2) {
		//swaps two objects
		if ((index1 > index2 ? index2 < 0 : index1< 0) || (index1 > index2 ? index1 > data.length : index2 > data.length)) {
			return false;
		}
		
		T one = data[index1];
		data[index1] = data[index2];
		data[index2] = one;
		one = null;
		return true;
	}
	
	public void sort(ArraySorter<T> sorter) {
		Array<T> temp = clone();
		clear();
		for ( int i=0 ; i<temp.getLength() ; i++ ) {
			add(temp.get(i));
			for ( int j=(getLength() - 1) ; j>0 ; j-- ) {
				if (sorter.comesBefore(data[j], data[j - 1])) {
					swap(j,j - 1);
					continue;
				} else {
					break;
				}
			}
		}
	}
	
	@Override
	public Array<T> clone() {
		return new Array<T>(data);
	}
	
	public T[] toArray() {
		return data.clone();
	}
	
	@Override
	public String toString() {
		String ret = "";
		for ( int i=0 ; i<data.length ; i++ ) {
			if (i == (data.length - 1)) {
				ret += data[i];
			} else {
				ret += data[i]+",";
			}
		}
		return ret;
	}
}