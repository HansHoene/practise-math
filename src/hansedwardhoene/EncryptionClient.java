// Program: EncryptionClient.java
// Written by: Hans-Edward Hoene
// Description: Creates GUI wich provides user capability to encrypt or decrypt files using input keys
//                   Revision History
// Date:                   By:               Action:
// ---------------------------------------------------
// 29 Feb 2015        	 HEH       				Created
// 5 Mar 2016			 HEH					Creation Finished

package hansedwardhoene;

import hansedwardhoene.cryptography.*;

import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/*filename: EncryptionClient.java*/
public class EncryptionClient extends java.awt.Frame {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private final static int VERSION = 1;
	@SuppressWarnings("unused")
	private final static int UPDATE = 1;
	@SuppressWarnings("unused")
	private final static String DATE = "1 March 2016";

	private Container set, work;
	private Label title1, title2, baseLabel, publicKeyLabel, privateKeyLabel, modulusLabel, checkKeysLabel, fileLabel, location;
	private Choice base;
	private TextField publicKey, privateKey, modulus, file;
	private Checkbox checkKeys;
	private JProgressBar progress;
	private JButton submit, encrypt, decrypt, fileChooser;
	//title2, fileLabel, location, file, chooseFile, progress, encrypt, decrypt: belong to work
	//else belong to set
	private MouseAdapter encDecClickEvent; //click events for encrypt and decrypt buttons
	private EncryptionStatusListener listener; //listeners for FileEncryptor enc
	
	private Font titleFont, regFont;
	private Border buttonBorder;
	
	private FileEncryptor enc;
	
	private boolean canClose; //does not allow window to close if false
	
	public EncryptionClient() {
		super("Encryption Client");
		canClose = true;
		setVisible(true);
		setLayout(null);
		setBounds(100, 100, 600, 500);
		addWindowListener(new WindowAdapter() {
			//listener for window closing
			public void windowClosing(WindowEvent e) {
				if (!canClose) return;
				e.getWindow().setEnabled(false);
				int opt = JOptionPane.showConfirmDialog(null, "Are you sure that you would like to exit?", "Close Application?", JOptionPane.YES_NO_OPTION);
				if (opt == JOptionPane.YES_OPTION) {
					System.exit(0);
				} else {
					e.getWindow().setEnabled(true);
					e.getWindow().requestFocus();
				}
			}
		});
		
		//variables, set up set, work, show set
		
		//variables
		titleFont = new Font("Arial", 0, 24);
		regFont = new Font("Arial", 0, 12);
		buttonBorder = BorderFactory.createLineBorder(Color.black, 2);
		enc = null;
		
		//set
		set = new Container();
		set.setBounds(10, 30, 600, 500);
		set.setVisible(true);
		
		title1 = new Label("Set Up Encryption/Decryption");
		title1.setBounds(5, 10, 450, 30);
		title1.setFont(titleFont);
		set.add(title1);
		
		baseLabel = new Label("Base: ");
		baseLabel.setBounds(5, 50, 50, 30);
		baseLabel.setFont(regFont);
		set.add(baseLabel);
		
		base = new Choice();
		base.add("Binary");
		base.add("Octal");
		base.add("Base Ten");
		base.add("Hexadecimal");
		base.setBounds(55, 50, 400, 30);
		set.add(base);
		
		publicKeyLabel = new Label("Public Key: ");
		publicKeyLabel.setBounds(5, 90, 70, 30);
		publicKeyLabel.setFont(regFont);
		set.add(publicKeyLabel);
		
		publicKey = new TextField();
		publicKey.setBounds(75, 90, 80, 30);
		set.add(publicKey);
		
		privateKeyLabel = new Label("Private Key: ");
		privateKeyLabel.setBounds(5, 130, 70, 30);
		privateKeyLabel.setFont(regFont);
		set.add(privateKeyLabel);
		
		privateKey = new TextField();
		privateKey.setBounds(75, 130, 80, 30);
		set.add(privateKey);
		
		modulusLabel = new Label("Modulus: ");
		modulusLabel.setBounds(5, 170, 70, 30);
		modulusLabel.setFont(regFont);
		set.add(modulusLabel);
		
		modulus = new TextField();
		modulus.setBounds(75, 170, 80, 30);
		set.add(modulus);
		
		checkKeysLabel = new Label("Check validity of keys? ");
		checkKeysLabel.setBounds(5, 210, 150, 30);
		checkKeysLabel.setFont(regFont);
		set.add(checkKeysLabel);
		
		checkKeys = new Checkbox();
		checkKeys.setBounds(155, 210, 30, 30);
		checkKeys.setState(true);
		set.add(checkKeys);
		
		submit = new JButton("Submit");
		submit.setBounds(10, 250, 300, 40);
		submit.setFont(new Font("Arial",0,24));
		submit.setBorder(buttonBorder);
		submit.setFont(titleFont);
		submit.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				try {
					loadEncryptor();
				} catch (IllegalArgumentException iae) {
					JOptionPane.showMessageDialog(null, iae.getMessage());
					loadSettings();
				}
			}
		});
		set.add(submit);
		
		//work
		work = new Container();
		work.setBounds(10, 30, 600, 500);
		work.setVisible(true);
		
		title2 = new Label("Encrypt or Decrypt File");
		title2.setBounds(5, 10, 450, 30);
		title2.setFont(titleFont);
		work.add(title2);
		
		fileLabel = new Label("File Path: ");
		fileLabel.setBounds(5, 50, 60, 30);
		fileLabel.setFont(regFont);
		work.add(fileLabel);
		
		file = new TextField();
		file.setBounds(65, 50, 450, 30);
		work.add(file);
		
		fileChooser = new JButton("...");
		fileChooser.setFont(regFont);
		fileChooser.setBounds(515, 50, 30, 30);
		fileChooser.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				String n = null;
				JFileChooser fc = new JFileChooser(n);
				int res = JFileChooser.ERROR_OPTION;
				try {
					res = fc.showOpenDialog(null);
				} catch (Throwable error) {}
				if (res == JFileChooser.APPROVE_OPTION) {
					try {
						file.setText(fc.getSelectedFile().getAbsolutePath());
					} catch (Throwable error) {}
				}
				fc = null;
				setVisible(true);
			}
		});
		work.add(fileChooser);
		
		progress = new JProgressBar(0, 100);
		progress.setMaximum(0);
		progress.setMaximum(100);
		progress.setBounds(10, 200, 400, 30);
		progress.setBackground(Color.white);
		progress.setForeground(Color.green);
		progress.setValue(0);
		progress.setStringPainted(true);
		progress.setVisible(false);
		work.add(progress);
		
		location = new Label("");
		location.setBounds(10, 230, 400, 30);
		location.setVisible(false);
		work.add(location);
		
		encrypt = new JButton("Encryt File");
		encrypt.setFont(titleFont);
		encrypt.setBounds(10, 110, 200, 40);
		encrypt.setBorder(buttonBorder);
		work.add(encrypt);
		
		decrypt = new JButton("Decryt File");
		decrypt.setFont(titleFont);
		decrypt.setBounds(250, 110, 200, 40);
		decrypt.setBorder(buttonBorder);
		work.add(decrypt);
		
		listener = (new EncryptionStatusListener() {
			public void updateProgress(int pr) {
				progress.setValue(pr);
			}
			public void locationChanged(String path) {
				location.setText(path);
			}
		});
		
		encDecClickEvent = (new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				canClose = false;
				fileChooser.setEnabled(false);
				file.setEnabled(false);
				encrypt.setEnabled(false);
				decrypt.setEnabled(false);
				progress.setVisible(true);
				location.setVisible(true);
				enc.addListener(listener);
				new Thread(new Runnable() {
					public void run() {
						try {
							if (e.getSource() == encrypt) {
								enc.encrypt(new java.io.File(file.getText()));
							} else if (e.getSource() == decrypt) {
								enc.decrypt(new java.io.File(file.getText()));
							}
						} catch (Throwable err) {
							JOptionPane.showMessageDialog(null, err.getMessage());
						} finally {
							enc.removeListener(listener);
							fileChooser.setEnabled(true);
							file.setEnabled(true);
							encrypt.setEnabled(true);
							decrypt.setEnabled(true);
							progress.setVisible(false);
							location.setVisible(false);
							progress.setValue(0);
							canClose = true;
						}
					}
				}).start();
			}
		});
		
		encrypt.addMouseListener(encDecClickEvent);
		decrypt.addMouseListener(encDecClickEvent);
		
		loadSettings();
		setVisible(true);
	}
	
	public void loadEncryptor() throws IllegalArgumentException {
		int baseNum, pubKeyNum, privKeyNum, modNum;
		String selectedBase = base.getSelectedItem();
		if (selectedBase.equals("Hexadecimal")) {
			baseNum = 16;
		} else if (selectedBase.equals("Base Ten")) {
			baseNum = 10;
		} else if (selectedBase.equals("Octal")) {
			baseNum = 8;
		} else if (selectedBase.equals("Binary")){
			baseNum = 2;
		} else {
			throw new IllegalArgumentException("Invalid Base Entered.");
		}
		try {
			pubKeyNum = Integer.parseInt(publicKey.getText());
		} catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid Public Key Entered.");
		}
		try {
			privKeyNum = Integer.parseInt(privateKey.getText());
		} catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid Private Key Entered.");
		}
		try {
			modNum = Integer.parseInt(modulus.getText());
		} catch (NumberFormatException err) {
			throw new IllegalArgumentException("Invalid Modulus Entered.");
		}
		enc = null;
		enc = new FileEncryptor(baseNum, pubKeyNum, privKeyNum, modNum, checkKeys.getState());
		invalidate();
		removeAll();
		add(work);
		revalidate();
		repaint();
	}
	
	public void loadSettings() {
		removeAll();
		invalidate();
		add(set);
		revalidate();
		repaint();
	}
	
	public static void main(String[] args) {
		new EncryptionClient();
	}

}