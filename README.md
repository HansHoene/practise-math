Hans-Edward Hoene

# Practise Math

## Licence Notes
This software uses a GNU General Public License v3.0.  In short, you may use
this software for private or commerical use.  In addition, you may modify and
distribute the source code.  However, whether or __not__ you do modify the
software, you must disclose the source code and you must include the same
licence.  You are also required to include a copyright notice.  Finally, if you
do make any changes to the source code, you must document and report them.

## About
Practise Math was originally developed in the winter of 2016 (I think?).  I
developed the tool to help my younger sister learn arithmetic.  I was an
unexperienced programmer, so much of the code is difficult to read and
uncommented.  Nevertheless, I have decided to publically post it as my younger
sister shows renewed interest in using it.

This was written in JAVA, so you must have JAVA installed to run this.  You must
have a JAVA compiler to compile this into a runnable program.  If you do not
wish to compile this on your own, look at "Tags" to select the most recent
release.  As long as JAVA is installed, you will be able to run the most recent
attached JAR file.

This tool allows you to practise your basic arithmetic skills such as addition,
subraction, multiplication, and division.

All history is saved.  The software will make a directory called "Hoene
Multipurpose Software Development Company" in the home directory upon startup.
All history is saved here.

Finally, I apologise for the mixture of American and British English.

## Future Work
I cannot really promise much change because I am a full-time univeristy graduate
student.  However, I do encourage issue/bug reporting from users.  I can try to
fix the easy stuff.  I also encourage other people to build upon my code, if
they desire.  Some ideas I have, which I may or may not have time to work on,
include a more personalised practice session and faster loading of playing
history.  I also plan on adding a Makefile in order to make compilation easier.
I have been using Eclipse Luna to code, build, and export projects.

If you make changes, keep the licence in mind.  To report changes, per the
licence requirement, I recommend making merge requests.
